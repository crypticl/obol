;;;;-*-lisp-*-
;;;;
;;;; Description: Utilities for Lobo, the Obol runtime.
;;;; Author: T�le Skogan
;;;; Copyright (C) 2003-2004,
;;;; Distribution:  See the accompanying file LICENSE.
;;;;

(in-package util)

(defparameter *verbosity* 2)

(defun set-verbosity ()
  (format t "~&New verbosity level (old=~A): " *verbosity*)
  (setq *verbosity* (read)))


(defun insert-list (pos list obj)
  "Insert obj into list at position pos and return new list."
  (let ((len (length list)))
    (assert (and (<= pos len) (>= pos -1)) () "pos=~A len=~A" pos len)
    ;;Special case if list is empty
    (if (= pos -1)
	(progn
	  (assert (null list))
	  (list obj))
      (append (butlast list (- len pos)) (list obj) (nthcdr pos list)))))

(defun delete-list (pos list)
  "Removes item at position pos from list and return new list. Assumes
there is an item at that position."
  (let ((len (length list)))
    (assert (and (< pos len) (>= pos 0)) () "pos=~A len=~A" pos len)
    (append (butlast list (- len pos)) (nthcdr (1+ pos) list))))
  

;; Only messages with importance above or equal to limit get printed.
;; Use the range [0 3] for normal messages.
;; 0 - old development debugging messages, not intended for normal debugging
;; 1 - normal debug
;; 2 - focused debug : during development of a new feature
;; 3 - normal. Important error messages, warnings etc
(defun formatv (level &rest args)
  "Important messages has high level, 3 is high, 0 is low. Supports Java style + to break up long string constants: (formatv 2 t dd + eee)"
  (let* ((stream (first args))
	 (format-string 
	  (apply #'concatenate 
		 (append (list 'string) (get-format-string (cdr args)))))
	 (format-args (get-format-args (cdr args)))
	 (all (append (list stream) (list format-string) format-args)))

    (when (>= level *verbosity*)
      (apply #'format all))))

(defun f2 (&rest args)
  "Short name version of formatv"
  (apply #'formatv 2 args))

(defun f1 (&rest args)
  "Short name version of formatv"
  (apply #'formatv 1 args))

(defun f3 (&rest args)
  "Short name version of formatv"
  (apply #'formatv 3 args))

(defun f0 (&rest args)
  "Short name version of formatv"
  (apply #'formatv 0 args))

(defun test-formatv ()
  (assert (string= (formatv 2 nil "Common Lisp") "Common Lisp"))
  (assert (string= (formatv 2 nil "Lisp is" '+ " the red pill") 
		   "Lisp is the red pill"))
  (assert (string= (formatv 2 nil "Lisp " '+ "is " '+ "the " '+ "red " '+ 
			    "pill")  "Lisp is the red pill"))
  (assert (string= (formatv 2 nil "A ~A, " '+ "a ~A, " '+ 
			    "a ~A ~A ~A" 1 2 1 2 3) "A 1, a 2, a 1 2 3"))
  "formatv OK.")
  
(defun get-format-args (args)
  (let ((prev '+)
	(prev-prev '+)
	(res nil))
    (dolist (arg args (reverse res)) 
      (cond
       ;; Do not insert parts of the format string
       ((equal prev '+)
	(setf prev-prev prev)
	(setf prev arg))
       ;; expect the next to be a format string
       ((equal arg '+)
	(setf prev-prev prev)
	(setf prev '+))
       ;; The rest is all format args.
       ((and (equal prev-prev '+) (stringp prev))
	(setf res (insert-list 0 res arg)))))))  
  
(defun get-format-string (args)
  (let ((prev '+)
	(res nil))
    (dolist (arg args (reverse res)) 
      (cond
       ((and (equal prev '+) (stringp arg))
	(setf res (insert-list 0 res arg))
	(setf prev arg))
       ((equal arg '+)
	(setf prev '+))))))
	    

(defun last1 (lst)
  "Returns the last element of a list, not the last cons cell"
  (first (last lst)))

