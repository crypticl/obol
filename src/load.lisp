;;;;-*-lisp-*-
;;;;
;;;; Description: load file.
;;;; Author: T�le Skogan 
;;;; Copyright (C) 2003-2004,
;;;; Distribution:  See the accompanying file LICENSE.
;;;;

(in-package cl-user)

(load "C:\\crypticl\\src\\load")

(let ((files '("packages"
	       "util"
	       "lobo"
	       "chat"
	       "lib-chat"
	       ;;"rpc"
	       )))
  (dolist (file files)
    (let* ((path
	    (make-pathname 
	     :device (pathname-device *load-pathname*)
	     :directory (directory-namestring *load-pathname*)))
	   (module (merge-pathnames file path)))
      (compile-file-if-needed module)
      (load module))))


