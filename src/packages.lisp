;;;;-*-lisp-*-
;;;;
;;;; Description: Packages for Lobo, the Obol runtime.
;;;; Author: T�le Skogan
;;;; Copyright (C) 2003-2004,
;;;; Distribution:  See the accompanying file LICENSE.
;;;;

(in-package cl-user)

(defpackage util
  (:use common-lisp excl)
  (:export
   insert-list
   delete-list
   f0
   f1
   f2
   f3
   set-verbosity
   last1))

(defpackage lobo
  (:use common-lisp excl util)
  (:export
   *rpc-server-port*
   lobo
   get-result
   send-msg-to-script
   continue-script
   load-script
   kill-script
   parse-obol
   add-event
   parse-spki
   generate-nonce
   send-msg
   format-msg-for-send))

(defpackage rpc
  (:use common-lisp excl lobo util)
  (:export start-rpc-server *rpc-server-port*))

(defpackage chat
  (:use common-lisp excl lobo util rpc)
  (:export safe-chat start-chat-telnet-server))

(defpackage lib-chat
  (:use common-lisp excl lobo util)
  (:export safe-chat start-lib-chat-telnet-server))

