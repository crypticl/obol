;;;;-*-lisp-*-
;;;;
;;;; Description:  Secure chat using Lobo as linked library
;;;; Author: T�le Skogan
;;;; Copyright (C) 2003-2004,
;;;; Distribution:  See the accompanying file LICENSE.
;;;;
;;;; Testing:
;;;; -start server with (lobo '("prog/lib-chat-auth-s.obol"))
;;;; -connect to chat telnet server: choose b first, then a.

;;TO DO

(in-package lib-chat)

(defvar *chat-port* 3050)

(defun start-lib-chat-telnet-server (&optional port)
  (if port
      (setf *chat-port* port)
    (setf port (incf *chat-port*)))
  (f2 t "~&Opening port ~A for lib-chat telnet server." port)
  (let ((passive (socket:make-socket :connect :passive
				     :local-host "127.1"
				     :local-port port
				     :reuse-address t)))
    (mp:process-run-function 
	`(:name "chat-telnet-server" 
		:initial-bindings ((*package* . ',*package*)))
      #'(lambda (pass)
	  (let ((count 0))		;use script id
	    (unwind-protect
		(loop
		  (f2 t "~&Ready to accept connection on port ~A" port)
		  (let ((con (socket:accept-connection pass)))
		    ;; Must bind *package* to current package, otherwise
		    ;; the reader will intern symbols in the default package
		    ;; (cl-user) and not in the same package as the 
		    ;; interpreter.
		    (mp:process-run-function
			`(:name ,(format nil "chat~d" (incf count))
				:initial-bindings
				((*standard-output* . ',*standard-output*)
				 (*package* . ',*package*)
				 (*standard-input* . ',*standard-input*)
				 ,@excl:*cl-default-special-bindings*))
		      #'handle-connection con)))
	      (ignore-errors (close pass :abort t)))))
      passive)))


(defun handle-connection (con)
  (unwind-protect
      (progn 
	(format con 
		"~&Choose (1 chat-a, 2 chat-b):")
	(finish-output con)
	(case (read (make-string-input-stream (read-line con)))
	  (1 (safe-chat con
			"a" 
			"prog/lib-chat-auth-a.obol"))
	  (2 (safe-chat con 
			"b"
			"prog/lib-chat-auth-b.obol"))))
    (ignore-errors (close con :abort t))))


(defun file-as-string (path)
  "Return a text file as a string. Used to avoid symbol interning problems."
  (let ((value ""))
    (with-open-file (stream path :direction :input)
      (do ((line (read-line stream nil)
		 (read-line stream nil)))
	  ((null line) value)
	;; Adds newline to make debugging easier.
	(setf value (concatenate 'string value line (format nil "~%")))))))


(defun safe-chat (display-con id auth-script-path)
  "An secure IRC application using Lobo."
  (let ((session-key)
	(receive-id)
	(send-id))   
    (setf session-key (init-chat auth-script-path))
    (f2 t "~&safe-chat[~A]: Got session key ~A" id session-key)        
    (multiple-value-setq (send-id receive-id)
			(load-channel id "AES"))
    (f2 t "~&safe-chat: id=~A send-id=~A receive-id=~A" id send-id receive-id)
    
    (lobo:send-msg-to-script receive-id session-key)
    (lobo:send-msg-to-script send-id session-key)
    
    (loop
      (format display-con "~&secure-chat[~A]>" id)
      (force-output display-con)
      (let ((msg (chat-multiplex display-con send-id receive-id)))
	(when (string= msg "change-alg")
	  (lobo:kill-script receive-id)
	  (lobo:kill-script send-id)
	  (multiple-value-setq (send-id receive-id)
	    (load-channel id "AES"))
	  (f2 t "~&safe-chat: id=~A send-id=~A receive-id=~A" 
	      id send-id receive-id)
	  (lobo:send-msg-to-script receive-id session-key)
	  (lobo:send-msg-to-script send-id session-key))
	(when (string= msg "exit")
	  (lobo:kill-script receive-id)
	  (lobo:kill-script send-id)
	  (return))))))


(defun load-channel (id alg)
  (let ((receive-id)
	(send-id))
    (if (string= "a" id)
	(progn
	  (setf receive-id 
	    (load-channel-script "prog/lib-chat-receive.obol"
				 "a"
				 "b"
				 alg))
	  (setf send-id 
	    (load-channel-script "prog/lib-chat-send.obol"
				 "a"
				 "b"
				 alg)))
      (progn
	(setf receive-id 
	  (load-channel-script "prog/lib-chat-receive.obol"
			       "b"
			       "a"
			       alg))
	(setf send-id 
	  (load-channel-script "prog/lib-chat-send.obol"
			       "b"
			       "a"
			       alg))))
    (values send-id receive-id)))
				  

(defun load-channel-script (path local-addr remote-addr alg)
  (let ((script 
	(format nil 
	    "(believe L \"~A\" address) (believe R \"~A\" address) (believe K (load) shared-key ((alg ~A) (size 128))) ~A" 
	    local-addr
	    remote-addr
	    alg
	    (file-as-string path))))
    (f1 t "~&load-channel-script: script= ~A" script)
    
  (lobo:load-script
   (lobo:parse-obol nil script))))
  
(defun init-chat (auth-script-path)
  (let ((key-exchange-id)
	(session-key)
	(auth-script (file-as-string auth-script-path)))
    (setf key-exchange-id 
      (lobo:load-script 
       (lobo:parse-obol nil auth-script)))
    
    ;; Wait for OK from key-exchange script. This comes via the Obol RETURN
    ;; expression.
    (loop
      (setf session-key (lobo:get-result key-exchange-id))
      (mp:process-sleep 0.3)
      (when session-key
	(return 
	  (car 
	   (parse-spki
	    (second session-key))))))))


(defun chat-multiplex (display-con send-id receive-id)
  "Multiplex client calls and input from receive script."
  (let ((msg))
    (loop
      ;; Check if data on socket. It may have been closed by the client and 
      ;; then we abort.
      (unless (open-stream-p display-con)
	(f2 t "~&rpc-multiplex: Stream to irc client closed. Exit.")
	(return))
      
      ;; Check if data from display.
      (when (listen display-con)
	(f2 t "~&chat-multiplex: Got data from display.")
	(setf msg (read-line display-con))   
	;; remove last char (probably \n from telnet's \r\n line termination.
	(setf msg (subseq msg 0 (- (length msg) 1)))
	(if (match-regexp "exit" msg)
	    (return-from chat-multiplex "exit")
	  (lobo:send-msg-to-script send-id msg))
	(return))
      
      ;; Check if data from receive script.
      (setf msg (second (lobo:get-result receive-id)))
      (when msg
	(setf msg (octets-to-string (car (lobo:parse-spki msg))))
	(f2 t "~&rpc-multiplex[~A]: Got msg from receive script=~S" 
	    receive-id msg)
	
	(when (match-regexp "change-alg" msg)
	    (return-from chat-multiplex "change-alg"))
	
	;; Send data to display
	(format display-con "~%~A" msg)
	(force-output display-con)
	(return))
      
      (mp:process-sleep 0.5))))
