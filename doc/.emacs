;; Binding *.lisp files to Common Lisp mode. Does this by adding cons cell
;; to global variable. Must come before we load lisp.
(setq auto-mode-alist (cons '("\\.lisp?$" . common-lisp-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.emacs?$" . emacs-lisp-mode) auto-mode-alist))

(setq fi:find-tag-lock nil)

(setq fi:lisp-mode-hook
	  (function
	   (lambda ()
	     (let ((map (current-local-map)))
	       (define-key map "\e."	'fi:lisp-find-definition)
	       (define-key map "\e,"	'fi:lisp-find-next-definition)))))

(setq fi:common-lisp-buffer-name "*common-lisp*"
      fi:common-lisp-directory "C:/crypticl/src/"
      fi:common-lisp-image-name "C:/Programfiler/acl62/mlisp"
      fi:common-lisp-image-file "C:/Programfiler/acl62/mlisp"
      fi:common-lisp-image-arguments nil
      fi:common-lisp-host "localhost")

;; This function starts up lisp with your defaults.
(load "C:/Programfiler/acl62/eli/fi-site-init")
(defun run-lisp ()
  (interactive)
  (fi:common-lisp fi:common-lisp-buffer-name
                  fi:common-lisp-directory
                  fi:common-lisp-image-name
                  fi:common-lisp-image-arguments
                  fi:common-lisp-host)
  (define-key fi:inferior-common-lisp-mode-map "\M-n" 'fi:push-input)
  (define-key fi:inferior-common-lisp-mode-map "\M-p" 'fi:pop-input))

(global-set-key [f12] 'run-lisp)

;;Mapping M-p to scroll through previous commands
(global-set-key "\M-p" 'fi:pop-input)

;;Mapping M-n to scroll through next commands
(global-set-key "\M-n" 'fi:push-input)

;;Making C-x-x switch to *common-lisp* buffer
(global-set-key "\C-x\C-x" '(lambda () (interactive) 
			     (switch-to-buffer "*common-lisp*")))

;; Get c-comment-ident
;;(require 'cc-cmds)

(require 'hyperspec)
(setq common-lisp-hyperspec-root "file:C:/emacs/doc/HyperSpec/")

(defvar use-home)
(setq use-home "C:/emacs/")

(setq acldoc-local-root "C:/Programfiler/acl62")
(setq acldoc-use-local t)

(setq load-path (append (list 
			 (concat use-home "site-lisp/acldoc")
			 (concat use-home "")
			 (concat use-home "w3/lisp"))
                        load-path))

(require 'w3-auto)
(require 'acldoc)

(global-set-key [f1] 'common-lisp-hyperspec)
(global-set-key [S-f1]
		'(lambda (chapter)
		  (interactive "P")
		  (w3-fetch (concatenate 'string
			      common-lisp-hyperspec-root
			      (if chapter
				  (format "Body/chap-%d.htm" chapter)
				"Front/Contents.htm")))))


(global-set-key [(control f1)] 
		'(lambda ()
		   (interactive)
		  (ignore-errors
		   (require 'url)
		   (require 'acldoc)
		   (unless acldoc-index-alist (acldoc-build-index))
		   (acldoc (thing-at-point 'symbol)))))

(defun last-command ()
  (interactive)
  (print (format "Last command:%S" last-command)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Functions to return to the previous position of point after "long jump",
;; i.e. after a call to beginning-of-buffer, fi:lisp-find-definition etc.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defvar *last-point* nil)

(defun store-point ()
  "queue sematics"
  (interactive)
  (setq *last-point* (append (list (point)) *last-point*)))

(defun pop-point ()
  "pops last point, unless there is only one left. If so, only return it (no pop)"
  (if (> (length *last-point*) 1)
      (prog1
	  (car *last-point*)
	(setq *last-point* (cdr *last-point*)))
    (car *last-point*)))
      
(defun return-to-point ()
  (interactive)
  (goto-char (pop-point)))

(global-set-key [f5] 'store-point)
(global-set-key [f6] 'return-to-point)			    

(defadvice end-of-buffer (before jump-return ())
  ""
  (store-point))
(ad-activate 'end-of-buffer)

(defadvice beginning-of-buffer (before jump-return ())
  ""
  (store-point))
(ad-activate 'beginning-of-buffer)

(defadvice fi:lisp-find-definition (before jump-return ())
  ""
  (store-point))
(ad-activate 'fi:lisp-find-definition)

(defadvice isearch-forward (before jump-return ())
  ""
  (store-point))
(ad-activate 'isearch-forward)

;; Start lisp when emacs starts
;;(run-common-lisp)

(setq auto-mode-alist (cons '("\\.py$" . python-mode) auto-mode-alist))
(setq interpreter-mode-alist (cons '("python" . python-mode)
                                    interpreter-mode-alist))
(autoload 'python-mode "python-mode" "Python editing mode." t)

(global-font-lock-mode t)
(setq font-lock-maximum-decoration t)
(transient-mark-mode 1)
(show-paren-mode 1)

(column-number-mode 1)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Make telent.el work on windows. Requires telnet.exe from cygwin.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq telnet-program "c:/cygwin/bin/telnet.exe")
(defvar *telnet-buffer-count* 0)
(require 'telnet)

(defun telnet (host-port)
  "Run as M-x telnet HOST PORT
Open a network login connection to host named HOST on port PORT.
Communication with HOST is recorded in a buffer *telnet-HOST:PORT-<count>*
where count is a global counter for this type of connections.
This functioned is a modified version of telnet.el, see C-h m telnet-mode.
Author: http://www.khngai.com/emacs/cygwin.php"
  (interactive "sOpen telnet connection to host(+ port): ")
  (let* ((comint-delimiter-argument-list '(?\  ?\t))
	 (host (comint-arguments host-port 0 0))
	 (port (comint-arguments host-port 1 1))
	 (name
	  (concat "telnet-" host ":" port "-"
		  (number-to-string (incf *telnet-buffer-count*)))) 	 
	 (buffer (get-buffer (concat "*" name "*")))
	 process)
    (cond ((string-equal system-type "windows-nt")
	   (setq telnet-new-line "\n")))
    (if (and buffer (get-buffer-process buffer))
	(pop-to-buffer (concat "*" name "*"))
      (pop-to-buffer (make-comint name telnet-program nil host port))
      (setq telnet-remote-echoes nil)
      (setq process (get-buffer-process (current-buffer)))
      (set-process-filter process 'telnet-initial-filter)
      (accept-process-output process)      
      (telnet-mode)
      (setq comint-input-sender 'telnet-simple-send)
      ;; Get echo of input
      (setq telnet-remote-echoes nil))))
