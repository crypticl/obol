;;;;-*-lisp-*-
;;;;
;;;; Description: Lobo, the Obol runtime.
;;;; Author: T�le Skogan 
;;;; Copyright (C) 2003-2004,
;;;; Distribution:  See the accompanying file LICENSE.
;;;;
;;;; To understand the code, the function evaluate is a good starting
;;;; point. You can experiment with individual primitives using the function
;;;; obol which will give you an Obol REPL.

;;TO DO

(in-package lobo)

(defparameter *event-queue* nil "Global event queue.")
(defparameter *event-queue-gate* nil "If open the event queue is alive.")

(defparameter *scripts* nil "All Obol scripts.")
(defparameter *scripts-lock* nil)

(defparameter *in-msg-queue* nil)
(defparameter *out-msg-queue* nil)
(defparameter *network-msg-queue* nil)

(defparameter *yield* "yield:nil"
  "The return value used when a function wants to signal yield to its caller.")

(defparameter *lobo-started* nil "True when Lobo started, nil otherwise.")

;; Give access to Common Lisp functions. 
(defparameter *obol-procs*
    '((type-of type-of)			
      (+ +)
      (not not)
      (eq eq)
      (eql eql)))

;; Creates unique id's inside a runtime.
(defparameter *script-id* 0)
(defparameter *scripts-id-lock* nil)
(defun next-script-id ()
  (mp:with-process-lock (*scripts-id-lock*)
    (incf *script-id*)))


;;;;;;;;;
;;; The queue holding the Obol scripts.

(defclass ScriptQueue ()
  ((q :accessor q :initform nil)
   ;; pointer to the active script (=head of queue). Traversing low to high.
   (current :accessor current :initform -1)))

(defun make-ScriptQueue ()
  (make-instance 'ScriptQueue))

(defmethod emptyp ((obj ScriptQueue))
  (null (q obj)))

(defmethod add-script ((obj ScriptQueue) script)
  "Add script at the end of the queue: at current pos and then increase current pos. Must syncronize access. If the script already exists, add"
  (mp:with-process-lock (*scripts-lock*)
    (setf (q obj) (insert-list (current obj) (q obj) script))
    (incf (current obj))))

(defmethod get-script ((obj ScriptQueue) script-id)
  (dolist (s (q obj))
    (when (= script-id (id s)) 
      (return s))))

(defun script-add-nonce (script-id nonce)
  (add-nonce (get-script *scripts* script-id) nonce))

(defun script-old-nonce-p (script-id nonce)
  "Return true if this script has  seen the nonce before"
  (old-nonce-p (get-script *scripts* script-id) nonce))

(defun write-script (script-id string)
  (format (str (get-script *scripts* script-id)) string))

(defmethod script-exist-p-internal ((obj ScriptQueue) script-id)
  "Return true if a script with that id."
  (dolist (s (q obj))
    (when (= script-id (id s)) 
      (return script-id))))

(defun script-exist-p (script-id)
  (when script-id
    (script-exist-p-internal *scripts* script-id)))

(defmethod add-exp-to-script ((obj ScriptQueue) id exp-list)
  "Add expressions to exisiting script."
  (mp:with-process-lock (*scripts-lock*)
    ;; Loop the queue looking for right id. When found, append list,
    ;; update script-object.
    (do* ((len (length (q obj)))
	  (pos 0 (1+ pos))
	  (script (nth pos (q obj)) (nth pos (q obj))))
	((>= pos len) (error "Script id=~A not found" id)) ;script not found
      (when (equal id (id script))
	(add-exp script exp-list)
	(return)))))			;breaks the loop
	
(defmethod print-object ((obj ScriptQueue) stream)
  (mp:with-process-lock (*scripts-lock*)
    (format stream
	    "~&ScriptQueue: len=~A current=~A current-script-id=~A" 
	    (length (q obj)) (current obj) 
	    (if (get-current-script obj)
		(id (get-current-script obj))
	      nil))
    (dolist (s (q obj))
      (format stream "~&~A" s))))


(defmethod delete-script ((obj ScriptQueue) pos)
  (mp:with-process-lock (*scripts-lock*)
    (f2 t "~&delete-script: pos=~A" pos)
    (if (null (q obj))			;empty list
	nil
      (let ((element (nth pos (q obj))))
	(setf (q obj) (delete-list pos (q obj)))
	;; Adjust current
	(when (<= pos (current obj))
	  (decf (current obj)))
	element))))

(defmethod delete-current-script ((obj ScriptQueue))
  (delete-script obj (current obj)))

(defmethod get-current-script ((obj ScriptQueue))
  "Return nil if no scripts. Is locking necssary for a read?"
  (mp:with-process-lock (*scripts-lock*)
    (if (emptyp obj)
	nil
      (nth (current obj) (q obj)))))

(defun get-current-script-id ()
  "Returns the id of the current script. Used in eval-return"
  (when *scripts*
    (let ((current (get-current-script *scripts*)))
      (when current
	(id current)))))
      
(defun gid ()
  (get-current-script-id))
		  
(defun get-current-script-rpc-handle ()
  "Returns the rpc-handle of the current script. Used in eval-return"
  (let ((current (get-current-script *scripts*)))
    (rpc-handle current)))

(defmethod get-current-stream ((obj ScriptQueue))
  "Return *standard-output* if no scripts. Need locking? Note that with-process-lock will not cause deadlock despite that get-current-script also tries to take the same lock."
  (mp:with-process-lock (*scripts-lock*)
    (let ((current-script (get-current-script obj)))
      (str current-script))))


(defun get-stream-string (id &optional (copyp t))
  "If copyp is false, as for the telnet clients, this function will block by polling until some data is available on the stream. An alternative solution would be to use a gate that could be opened by eval-print and anyone else printing to the stream. Unfortunately, this function blocks the prompt after send instead."
  (if copyp
      (let 
	  ((string (get-output-stream-string (str (get-script *scripts* id)))))
	;; Write back. Must use princ and not print to avoid trouble with
	;; escape charcters when doing multiple calls.
	(princ string (str (get-script *scripts* id)))
	string)
    ;; NB! This clears the stream buffer. Loop until there is some output.
    (do ((string (get-output-stream-string (str (get-script *scripts* id)))
		 (get-output-stream-string (str (get-script *scripts* id)))))
	((string/= string "") string)
      ;; Avoid busy poll
      (mp:process-sleep 0.1))))


(defun get-script-stream (id)
  (get-stream-string id))

(defun gss (id)
  (get-script-stream id))

(defmethod active-scripts-p ((obj ScriptQueue))
  (mp:with-process-lock (*scripts-lock*)
    (dolist (script (q obj))
      (unless (or (donep script) (blockedp script) (abortedp script))
	(return-from active-scripts-p t)))))     

(defmethod unblock-all-scripts ((obj ScriptQueue))
  (mp:with-process-lock (*scripts-lock*)
    (dolist (script (q obj))
      (mark-not-blocked script))))

(defun unblock-scripts ()
  (unblock-all-scripts *scripts*)
  (add-event "SCRIPT"))

(defun unblock-script (script-id)
  (mp:with-process-lock (*scripts-lock*)
    (mark-not-blocked (get-script *scripts* script-id))))     
  
(defun mark-script-done (script-id)
  (mp:with-process-lock (*scripts-lock*)
    (mark-done (get-script *scripts* script-id))))     

(defun mark-script-aborted (script-id)
  (mp:with-process-lock (*scripts-lock*)
    (mark-aborted (get-script *scripts* script-id))))

(defun unmark-script-aborted (script-id)
  (mp:with-process-lock (*scripts-lock*)
    (mark-not-aborted (get-script *scripts* script-id))))

(defmethod next-active-script ((obj ScriptQueue))
  "Move current to next active script and return the script. If current is active, return current. If no active scripts, return nil."
  (mp:with-process-lock (*scripts-lock*)
    (do* ((len (length (q obj)))
	  (count 0 (1+ count))
	  (pos (current obj) (mod (1+ pos) len)))
	((or (>= count len) 
	     (activep (nth pos (q obj))))
	 (setf (current obj) pos)
	 (f1 t "~&next-active-script: found id=~A" 
		  (id (nth pos (q obj))))
	 (nth pos (q obj))))))

(defmethod advance-current ((obj ScriptQueue))
  "Advance current to next script. May be itself if only one object in the 
queue. Used when the current script yields."
  (mp:with-process-lock (*scripts-lock*)
    (let ((len (length (q obj))))
      (setf (current obj) (mod (1+ (current obj)) len)))))  

(defmethod get-next-exp ((obj ScriptQueue))
  "Get next toplevel expression from the active script."
  (mp:with-process-lock (*scripts-lock*)
    (let ((current (next-active-script obj)))
      (script-get-next-exp current))))

(defun receive-msg-from-application (script-id)
  "Pop the message."
  (let ((script (get-script *scripts* script-id)))
    (prog1
	(from-application script)
      (setf (from-application script) nil))))

(defun add-msg-from-application (script-id msg)
  (setf (from-application (get-script *scripts* script-id)) msg))

(defun script-add-result (script-id result)
    (let ((script (get-script *scripts* script-id)))
      (enqueue-result script result)))

(defun script-get-result (script-id)
    (let ((script (get-script *scripts* script-id)))
      (dequeue-result script)))

(defun script-no-result-p (script-id)
    (let ((script (get-script *scripts* script-id)))
      (no-result-p script)))


;;;;;;;;;;;;;;;
;;; Individual Obol scripts

(defclass ObolScript ()
  ((script-env :accessor script-env
	       :initform (empty-script-env))
   (script :accessor script
	   :initarg :script)
   (id :accessor id 
       :initarg :id)
   (str :accessor str 
	:initarg :str)
   (from-application :accessor from-application
		     :initarg :from-application
		     :initform nil)
   (result :accessor result
	   :initform (make-instance 'mp:queue))
   (is-blocked :accessor is-blocked 
	       :initform nil)
   (is-aborted :accessor is-aborted 
	       :initform nil)
   (nonces :accessor nonces 
	   :initarg :nonces)
   (rpc-handle :accessor rpc-handle
	       :initarg :rpc-handle)
   (num-toplevels :accessor num-toplevels 
		  :initarg :num-toplevels)
   (next-exp :accessor next-exp 
	     :initform 0 )))

(defun make-ObolScript (script &key id rpc-handle from-application)
  (let ((num-toplevels (length script))) ;; num toplevel expressions
    (make-instance 'ObolScript :script script 
		   :id (or id (next-script-id))
		   :rpc-handle rpc-handle
		   :str (make-string-output-stream)
		   :from-application from-application
		   :nonces (make-hash-table :test #'equalp)
		   :num-toplevels num-toplevels)))

(defmethod print-object ((obj ObolScript) stream)
  (format stream "<ObolScript: id=~A" (id obj))
  (format stream "~& exps=~A" (script obj))
  (format stream "~& nonces=~A" (nonces obj))
  (format stream "~& from-application=~A" (from-application obj))
  (format stream "~& is-blocked=~A is-aborted=~A next-exp=~A" 
	  (is-blocked obj) (is-aborted obj) (next-exp obj)))


(defmethod dequeue-result ((obj ObolScript))
  (mp:dequeue (result obj) :wait t))

(defmethod enqueue-result ((obj ObolScript) result)
  (mp:enqueue (result obj) result))

(defmethod no-result-p ((obj ObolScript))
  (mp:queue-empty-p (result obj)))

(defmethod add-nonce ((obj ObolScript) nonce)
  (setf (gethash nonce (nonces obj)) nonce))

(defmethod old-nonce-p ((obj ObolScript) nonce)
  "Return true if this script has seen this nonce before, nil otherwise."
  (gethash nonce (nonces obj)))

(defmethod add-exp ((obj ObolScript) exp-list)
  "Adds expressions to script. May change the state of the script. Relies on external synchronization."
  (f1 t "~&add-exp: exp-list=~A" exp-list)
  (f1 t "~&add-exp: scr before=~A len=~A next-exp=~A"  
      (script obj) (num-toplevels obj) (next-exp obj))
  
  (setf (script obj) (append (script obj) exp-list))
  (setf (num-toplevels obj) (+ (num-toplevels obj) (length exp-list)))
  
  (f1 t "~&add-exp: scr after=~A len=~A nexp-exp=~A"  
	   (script obj) (num-toplevels obj) (next-exp obj)))

(defmethod activep ((obj ObolScript))
  "A script is only active if its not done and not blocked and not aborted."
  (not (or (donep obj) (blockedp obj) (abortedp obj))))

(defmethod donep ((obj ObolScript))
  "Returns true when the script have finished"
  (>= (next-exp obj) (num-toplevels obj)))

(defmethod mark-done ((obj ObolScript))
  "Mark the script as done. Used on timeout."
  (setf (next-exp obj) (num-toplevels obj)))
  
(defmethod blockedp((obj ObolScript))
  "Returns true if the script is blocking, i.e. waiting for receive"
  (is-blocked obj))

(defmethod mark-blocked ((obj ObolScript))
  (setf (is-blocked obj) t))

(defmethod mark-not-blocked ((obj ObolScript))
  (setf (is-blocked obj) nil))

(defmethod mark-aborted ((obj ObolScript))
  (setf (is-aborted obj) t))

(defmethod mark-not-aborted ((obj ObolScript))
  (setf (is-aborted obj) nil))

(defmethod abortedp((obj ObolScript))
  "Returns true if the script has aborted"
  (is-aborted obj))

(defmethod script-get-next-exp ((obj ObolScript))
  "Get next expression from this script and advance the pointer"
  (prog1
      (nth (next-exp obj) (script obj))
    (incf (next-exp obj))))

(defmethod reset-exp-pointer ((obj ObolScript))
  (decf (next-exp obj)))

       
(defun undo-and-yield ()
  "Undo effects of current toplevel expression and yield to next active script"
  ;; The order matters: reset must come before we load a new script.
  (reset-exp-pointer (get-current-script *scripts*))
  ;; Remember to reset this status when unblocked.
  (mark-blocked (get-current-script *scripts*))
  (advance-current *scripts*))


(defun get-script-env ()
  "Returns script-env from active script"
  (script-env (get-current-script *scripts*)))

(defun store (script-env)
  "Store a script environment"
  (let ((current (car *scripts*)))
    (setf (script-env current) script-env)))


;;;;;;;;;;;;;;;;;;;;;
;;; The event queue

;; An instance of equeue represents a server and its queue.
;; A gate is used to indicate that data needing to be handled
;; is on the queue.
(defstruct equeue
  data
  lock
  gate
  process)

;; Anyone with access to an equeue item can add to its work queue by calling
;; equeue-add. After the data is added, the associated gate is opened
;; (an open gate indicates that data is waiting to be handled).
(defun equeue-add (eq item)
  (mp:with-process-lock ((equeue-lock eq))
    (setf (equeue-data eq) (nconc (equeue-data eq) (list item)))
    (mp:open-gate (equeue-gate eq)))
  item)

(defun add-event (event-type &rest args)
  (equeue-add *event-queue* (cons event-type args)))

(defun obol (&optional script-id)
  "Create an Obol REPL. Create a new script if none exists, otherwise append to an existing one."
  (in-package lobo)
  (unless (lobo-started-p)
    (lobo))
  (let ((exp nil)
	(id))
    (if (script-exist-p script-id)
	(setf id script-id)
      (setf id (load-script '())))
    (loop
      (format t "obol[~A]>" id)
      (setf exp (list (read)))
      (when (equal (car exp) :exit)
	(return))
      (add-event "ADD-EXP-TO-LIST" id exp)
      (add-event "SCRIPT")
      (mp:process-sleep 0.5)
      (format t "~A~&" (get-stream-string id nil)))))

(defun obol> (id)
  " Add an Obol expression to a script and return."
  (format t "obol[~A]>" id)
  (add-event "ADD-EXP-TO-LIST" id (list (read)))
  (add-event "SCRIPT"))


;;;;;;;;;;;
;;; Timers
;;; 
;;; Assume there is never more than one timer per script.
;;;
;;; For each timer store the time-out time

(defparameter *active-timers* nil)

(defun delete-timer (script-id)
  (remhash script-id *active-timers*))

(defun set-timer (script-id timeout)
  ;; timeout in seconds
  (setf (gethash script-id *active-timers*) 
    (+ (get-universal-time) timeout)))

(defun check-timeout ()
  "Mark all timed out scripts as done."
  (maphash 
   #'(lambda (id timeout)  
       (when (> (get-universal-time) timeout)
	 (mark-script-done id)
	 (delete-timer id)
	 (return-error id "RECEIVE timed out")
	 (write-script id "~&RECEIVE timed out.")))
   *active-timers*))


;;;;;;;;;;;;
;;; The event queue server
;;;
;;; create-equeue-server starts a server process running and returns
;;; the associated equeue item to which work items can be queued.
;;; The server process calls server-function on each element it retrieves
;;; from the queue.  When server-function returns :exit, the server
;;; process exits.
;;;
;;; Note that the main loop (the process that is set to 
;;; the value of (equeue-process eq)) waits on the gate being open
;;; (indicating unhandled data is present) and closes the gate when
;;; all available data is handled.
(defun create-equeue-server (dispatcher-function)
  (let ((eq (make-equeue)))
    (setf (equeue-lock eq) (mp:make-process-lock)
	  (equeue-gate eq) (mp:make-gate nil)
	  (equeue-process eq)
	  (mp:process-run-function 
	      `(:name "evl"
		:initial-bindings
		((*standard-output* . ',*standard-output*)
		 (*package* . ',*package*)
		 (*standard-input* . ',*standard-input*)
		 ,@excl:*cl-default-special-bindings*))
	    #'(lambda (leq df)
		(loop
		  (mp:process-wait "waitev"
				   #'mp:gate-open-p
				   (equeue-gate leq))
		  (let (e 
			run)
		    ;; Remove timed out scripts
		    (check-timeout)
		    (mp:with-process-lock ((equeue-lock leq))
		      (if* (null (equeue-data leq))
			 then (mp:close-gate (equeue-gate leq))
			 else (setq e (pop (equeue-data leq)))
			      (setq run t)))
		    (when (and run (eq :exit (funcall df e)))
		      (f2 t "~&Exiting event loop")
		      (return)))))
	    eq dispatcher-function))
    eq))


(defun dispatcher (event)
  "Calls the correct eventhandler for an event. This function implicitly represent the event-eventhandler binding. An event is a list where the first element is the type of the event."
  (f1 t "~&dispatcher:Got event=~A" event)
  (let ((type (first event)))
    ;; Could use case and symbols here
    (cond
     ((string= "SCRIPT" type) (internal-obol))
     ((string= "GOT-PACKET" type) (handle-got-packet event))
     ((string= "NOP" type) nil) 
     ((string= "EXIT" type) (handle-exit))
     ((string= "KEYWORD" type) (handle-keyword event))
     ((string= "FORMAT" type) (handle-format event))
     ((string= "ADD-EXP-TO-LIST" type) (handle-add-exp-to-list event))
     ((string= "ADD-PACKET" type) (handle-add-packet event))
     ((string= "MSG-FROM-APP" type) (handle-msg-from-app event))
     (t (error "Unknown event=~A" event)))))

(defun handle-msg-from-app (event)
  (let ((id (second event))
	(msg (third event)))
    (add-msg-from-application id msg)
    (unblock-scripts)))  
  
(defun handle-add-packet (event)
  "Handle add packet from rpc or prompt"
  (f1 t "~&handle-add-packet: event=~A" event)
  (let ((receiver (second event))	
	;; msg is encoded string, as encoded by send
	(msg (third event)))
    (deliver-incoming-packet receiver msg)))

(defun handle-got-packet (event)
  "Handle incoming packet from packet-server"
  (f1 t "~&handle-got-packet: event=~A" event)
  (let ((msg (second event))
	(receiver (third event)))
    (deliver-incoming-packet receiver msg)
    (add-event "SCRIPT")))
  
(defun handle-add-exp-to-list (event)
  (let ((id (second event))
	(exp-list (third event)))
    (add-exp-to-script *scripts* id exp-list)))

(defun handle-format (event)
  (let ((stream (second event))
	(args (cddr event)))
    (f0 t "~&handle-format:~A" (cons stream args))
    (apply #'format (cons stream args))	;extract arguments from list
    (finish-output stream)))

(defun handle-keyword (event)
  (let ((id (second event))
    	(keyword (third event)))
    (f1 t "~&handle-keyword:~S" keyword)

    (cond 	  
     ;;Remember that a Lisp keyword evaluates to itself so (equal "EXIT" :exit)
     ;; => t
     ((equal keyword :exit)  (handle-exit))
      ;; Will invoke eval-runtime-cmd on the current script.
     (t
      (add-exp-to-script *scripts* id (list keyword))
      (add-event "SCRIPT")))))
      

(defun handle-exit ()
  (clean)
  :exit)				;Breaks event loop


(defun clean ()
  "Close threads. Note that mp:process-kill forks off separate thread so a subsequent call to mp:process-name-to-process may still find the process that is being killed (for a while)."
  (setf *lobo-started* nil)
  (setf *script-id* 0)
  (ignore-errors
   (mp:process-kill (mp:process-name-to-process "irca")))
  (ignore-errors
   (mp:process-kill (mp:process-name-to-process "rpc1")))
  (ignore-errors
   (mp:process-kill (mp:process-name-to-process "rpc2")))
  (ignore-errors
   (mp:process-kill (mp:process-name-to-process "rpc-server")))
  (ignore-errors
   (mp:process-kill (mp:process-name-to-process "packet1")))
  (ignore-errors
   (mp:process-kill (mp:process-name-to-process "packet2")))
  (ignore-errors
   (mp:process-kill (mp:process-name-to-process "packet-server")))
  (ignore-errors
   (mp:process-kill (mp:process-name-to-process "tel1")))
  (ignore-errors
   (mp:process-kill (mp:process-name-to-process "tel2")))
  (ignore-errors
   (mp:process-kill (mp:process-name-to-process "telnet")))
  (ignore-errors
   (mp:process-kill (mp:process-name-to-process "chat-telnet-server")))
  (ignore-errors
   (mp:process-kill (mp:process-name-to-process "chat1")))
  (ignore-errors
   (mp:process-kill (mp:process-name-to-process "chat2")))
  (ignore-errors
   (mp:process-kill (mp:process-name-to-process "chat3")))
  (ignore-errors
   (mp:process-kill (mp:process-name-to-process "evl"))))
  

(defvar *lobo-telnet-port* 4050)

(defun start-telnet (&optional port)
  (if port
      (setf *lobo-telnet-port* port)
    (setf port (incf *lobo-telnet-port*)))
  (f2 t "~&Open port ~A for telnet." port)
  (let ((passive (socket:make-socket :connect :passive
				     :local-host "127.1"
				     :local-port port
				     :reuse-address t)))
    (mp:process-run-function 
	`(:name "telnet" :initial-bindings ((*package* . ',*package*)))
      #'(lambda (pass)
	  (let ((count 0))		;use script id
	    (unwind-protect
		(loop
		  (f2 t "~&Lobo ready to accept telnet connection on port ~A" 
		      port)
		  (let ((con (socket:accept-connection pass)))
		    ;; Must bind *package* to current package, otherwise
		    ;; the reader will intern symbols in the default package
		    ;; (cl-user) and not in the same package as the 
		    ;; interpreter.
		    (mp:process-run-function
			`(:name ,(format nil "tel~d" (incf count))
				:initial-bindings
				((*standard-output* . ',*standard-output*)
				 (*package* . ',*package*)
				 (*standard-input* . ',*standard-input*)
				 ,@excl:*cl-default-special-bindings*))
		      #'handle-connection con)))
	      (ignore-errors (close pass :abort t)))))
      passive)))
    
(defun handle-connection (con)
  (unwind-protect
      (progn 
	(format con 
		"~&Choose (1 dump stream, 2 new script):")
	(finish-output con)
	(case (read (make-string-input-stream (read-line con)))
	  (1 (dump-stream con))
	  (2 (new-script con))))
    (ignore-errors (close con :abort t))))

(defun dump-stream (con)
  (let ((id))
    (format con "~&Choose script:")
    (finish-output con)
    (setq id (read (make-string-input-stream (read-line con))))	
    (loop
      (format con "~A" (get-stream-string id nil))
      (finish-output con)		
      (mp:process-sleep 1))))

(defun new-script (con)
  (in-package lobo) ;;because of reader
  (let ((id (next-script-id))
	(exp))
    (add-script *scripts* (make-ObolScript nil :id id))
    (loop
      (format con "~&obol[~A]>" id)
      (finish-output con)
      
      (setq exp 
	(list (read (make-string-input-stream (read-line con)) nil nil)))
      (when (equal (car exp) :exit)
	(return))
	
      (format t "~&got exp='~S'" exp)
      (add-event "ADD-EXP-TO-LIST" id exp)		 
      (add-event "SCRIPT")
      (mp:process-sleep 1) ;simulate gate on output stream
      (format con "~A" (get-stream-string id nil))
      (finish-output con))))		



;;;;;;;;;;;;
;;; The interpreter and runtime

(defun lobo-started-p ()
  *lobo-started*)

(defun lobo (&optional paths)
  "Parse and run an Obol script or enter Obol read-eval-print loop.
Parameters:
   paths: a list of Obol scripts, i.e. '('prog/test.obol' 'prog/tmp.obol')
" 
  (format t "~&Starting Lobo.~%")
  (init)
  (dolist (path paths)
    (add-script *scripts* (make-ObolScript (parse-obol path))))
  (setf *lobo-started* t)
  ;; Add the first SCRIPT event. This will invoke the eventqueue.
  (add-event "SCRIPT"))

(defun exit-lobo ()
  (add-event "EXIT")
  (setf *lobo-started* nil)
  nil) ; Return nil to avoid return value from add-event in the REPLs.
     
(defun internal-obol ()
  (when (active-scripts-p *scripts*)
    (let ((exp (get-next-exp *scripts*))
	  (str (get-current-stream *scripts*)))  
      (f1 t "~&internal-obol[~A]: exp=~S" (gid) exp) 
      
      ;; hack to make telnet REPLs look right when blocking on receive. Also
      ;; used to abort a script after an error.
      (handler-case
	  (let ((object (interpreter exp (get-script-env) (empty-env) str)))
	    (cond
	     ((yield-p object)
	      "internal-obol:nil")
	     (t
	      (print object str))))

	(script-exit (con)
	  (f1 t "~&internal-obol: id=~A msg=~A" (script-id con) (msg con))
	  (return-result (script-id con) (msg con))
	  (error-exit-cleanup con))
	(script-error (con)
	  (return-error (script-id con) (msg con))
	  (error-exit-cleanup con)))
      
    ;; Add new event if more active scripts 
    (when (active-scripts-p *scripts*)
      (add-event "SCRIPT")))))

(defun error-exit-cleanup (con)
  (mark-script-aborted (script-id con))
  (advance-current *scripts*)
  ;; Write msg to script stream so obol>> can continue
  (write-script (script-id con) (msg con)))

  
(define-condition script-exit (condition)
  ((script-id :accessor script-id :initarg :script-id)
   (msg :accessor msg :initarg :msg)))

(defmethod print-object ((obj script-exit) stream)
  (format stream "<script-exit: script-id=~A msg=~A>" 
	  (script-id obj) (msg obj)))

(define-condition script-error (condition)
  ((script-id :accessor script-id :initarg :script-id)
   (msg :accessor msg :initarg :msg)))

(defmethod print-object ((obj script-error) stream)
  (format stream "<script-error: script-id=~A msg=~A>" 
	  (script-id obj) (msg obj)))

(defun parse-obol (&optional path string)
  (cond
   (path
    (with-open-file (stream path)
      (loop for exp = (read stream nil nil)
	  until (eq exp nil)
	  collect exp)))
   (string
    (with-input-from-string (stream string)
      (loop for exp = (read stream nil nil)
	  until (eq exp nil)
	  collect exp)))))


(defun init ()
  (setf *scripts* (make-ScriptQueue))
  (setf *scripts-lock* (mp:make-process-lock))
  (setf *script-id* 0)
  (setf *scripts-id-lock* (mp:make-process-lock))
  (setf *in-msg-queue* (make-MessageQueue))
  (setf *out-msg-queue* (make-MessageQueue))
  (setf *network-msg-queue* (make-MessageQueue))
  (mapc #'init-obol-proc *obol-procs*)
  (set-global-var t t)			;Handles single t in the code
  (set-global-var nil nil)		;Handles single nil in the code
  (setf *event-queue* (create-equeue-server #'dispatcher))
  (setf *event-queue-gate* (mp:make-gate t)) ;start open
  (setf *active-timers* (make-hash-table))
  (start-packet-server 9000)
;;;  (rpc:start-rpc-server)
  ;;(lib-chat:start-lib-chat-telnet-server 3050)
;;;  (chat:start-chat-telnet-server 3050)
  (start-telnet 9023))
 
(defun init-obol-proc (f)
  "Define binding between obol primitive and interpreter handler"
  (set-global-var (first f) (symbol-function (second f))))


(defun interpreter (exp script-env local-env str)
  (f1 t "~&Interpreter: '~S'" exp)
  (f0 t "~&env at start of interpreter: ~&~A" local-env)
  
  ;; No print. Will not print a variable at the top-level unless nested inside
  ;; a print expression.
  (restart-case (evaluate exp script-env local-env str)
    (error (new-exp)
	:report
	  (lambda (s)
	    (format s "Replace expression '~A'. " exp))
	:interactive 
	  (lambda ()
	    (format t "~&New Obol expression (old='~A'): " exp)
	    (list (read)))
      (evaluate new-exp script-env local-env str))))
     
  
(defun evaluate (exp script-env local-env str)   
  (f1 t "~&evaluate[~A]: ~S" (gid) exp)
  (cond
   ((keywordp exp) (eval-runtime-cmd exp script-env local-env))
   ((symbolp exp) (get-var exp script-env local-env str))
   ((atom exp) exp)
   ((case (first exp)
      (quote (second exp))		;NB! Not '.
      (progn (eval-progn exp script-env local-env str))
      (if (eval-if exp script-env local-env str))
      (while (eval-while exp script-env local-env str))
      (lambda (eval-lambda exp script-env local-env str))
      (let (eval-let exp script-env local-env str))
      (bind (eval-bind exp script-env local-env str)) 
      (load-script (eval-load-script exp script-env local-env str))
      (print (eval-print exp script-env local-env str))
      (format (eval-format exp script-env local-env str))
      (list (eval-list exp script-env local-env str))
      (nth (eval-nth exp script-env local-env str))
      (xor (eval-xor exp script-env local-env str))
      (- (eval-subtract exp script-env local-env str))

      (load (eval-load exp script-env local-env str)) 
      (store (eval-store exp script-env local-env str))     
      (believe (eval-believe exp script-env local-env str))
      (generate (eval-generate exp script-env local-env str))
      (equal (eval-equal exp script-env local-env str))
      (encrypt (eval-encrypt exp script-env local-env str))
      (decrypt (eval-decrypt exp script-env local-env str))
      (receive (eval-receive exp script-env local-env str))
      (return (eval-return exp script-env local-env str))
      (send (eval-send exp script-env local-env str))
      (sign (eval-sign exp script-env local-env str))
      (verify (eval-verify exp script-env local-env str))
      (diffie-hellman (eval-diffie-hellman exp script-env local-env str))
      (otherwise (eval-function exp script-env local-env str))))))

(defun eval-diffie-hellman(exp script-env local-env str)
  " (diffie-hellman key) (diffie-hellman key Y)"
  (let* ((key (get-attribute 
	       (interpreter (second exp) script-env local-env str)
	       "value"))
	 (y 
	  (when (third exp)
	    (get-attribute 
	     (interpreter (third exp) script-env local-env str)
	     "value")))
    	 (algorithm (clc:algorithm key))
	 (cipher (clc:new-instance algorithm)))
        
    (typecase key
      (clc:Diffie-HellmanKey
       (clc:init-Diffie-Hellman cipher key))
      (t (error "eval-diffie-hellman:Unknown key")))
    (if y
	(clc:get-secret-Diffie-Hellman cipher y)
      (clc:generate-random-Diffie-Hellman cipher))))


(defun eval-subtract (exp script-env local-env str)
  "(- var1 var2)" 
  (let ((exp1 (interpreter (second exp) script-env local-env str))
	(exp2 (interpreter (third exp) script-env local-env str))
	(val1)
	(val2)
	(was-byte-array nil)
	(res))
    (if (equal (type-of exp1) 'ObolVariable)
	(setf val1 (get-attribute exp1 "value"))
      (setf val1 exp1))
    (if (equal (type-of exp2) 'ObolVariable)
	(setf val2 (get-attribute exp2 "value"))
      (setf val2 exp2))
    ;; There is a short cut for doing -1 using logical operators.
    (unless (integerp val1)
      (setf was-byte-array t)
      (setf val1 (clc:octet-vector-to-integer val1)))
    (unless (integerp val2)
      (setf was-byte-array t)
      (setf val2 (clc:octet-vector-to-integer val2)))
    (setf res (- val1 val2))
    (if was-byte-array
	(clc:integer-to-octet-vector res)
      res)))

(defun eval-return (exp script-env local-env str)
  (let* ((attributes (get-attributes-from-expression exp))
	 (type (or (second (assoc 'type attributes)) 'exit)))
    (case type
      (exit (return-exit exp script-env local-env str))
      (continue (return-continue exp script-env local-env str))
      (error-continue (return-error-continue exp script-env local-env str))
      (error-exit (return-error-exit exp script-env local-env str))
      (t (error "Unknown return type ~S" type)))))

(defun get-return-value (exp script-env local-env str)
  "(RETURN result)"
    (format-msg-for-send (interpreter (second exp) script-env local-env str)))

(defun return-continue (exp script-env local-env str)
  "Returns the result, but no exit from the script."
  (return-result (get-current-script-id) 
		 (get-return-value exp script-env local-env str)))

(defun return-exit (exp script-env local-env str)
  "Returns result and exits the script. This is the default."
  (signal 'script-exit 
	  :script-id (get-current-script-id) 
	  :msg (get-return-value exp script-env local-env str)))

(defun return-error-continue (exp script-env local-env str)
  "Return error result, but continue with next expression if there is one."
  (return-error (get-current-script-id) 
		(get-return-value exp script-env local-env str)))

(defun return-error-exit (exp script-env local-env str)
  "Aborts the script."
  (signal 'script-error 
	  :script-id (get-current-script-id) 
	  :msg (get-return-value exp script-env local-env str)))          

(defun eval-sign (exp script-env local-env str)
  "(SIGN private-key data1 data2 ...). Return a usb8 BLOB."
  (f1 t "~&eval-sign: exp=~A" exp)
  (let* ((key-var (interpreter (second exp) script-env local-env str))
	 (key (get-attribute key-var "value"))       
	 (msg-elements (mapcar #'(lambda (element)
			  (interpreter element script-env local-env str))
		      (cddr exp)))	 
	 (var (format-msg msg-elements))
	 (signer 
	  (typecase key
	    (clc:RSAPrivateKey
	     (clc:new-instance "SHA-1withRSA"))
	    (t (clc:new-instance (clc:algorithm key)))))) 
    (f1 t "~&eval-sign: key=~A" key)
    (f1 t "~&eval-sign: data to sign=~A" var)
    (assert (subtypep (type-of var) 'simple-array))

    (clc:init-sign signer key)
    (format-msg (clc:sign signer var))))
    

(defun eval-verify (exp script-env local-env str)
  "(VERIFY public-key signature data1 data2 ...). Return t if OK, else throw 
error. The signature must be an usb8 BLOB created by eval-sign."
  (f1 t "~&eval-verify: exp=~A" exp)
  (let* ((key-var (interpreter (second exp) script-env local-env str))
	 (key (get-attribute key-var "value"))
	 (signature (interpreter (third exp) script-env local-env str))
	 (msg-elements (mapcar #'(lambda (element)
			  (interpreter element script-env local-env str))
		      (cdddr exp)))	 
	 (var (format-msg msg-elements))
	 (verifier
	  (typecase key
	    (clc:RSAPublicKey
	     (clc:new-instance "SHA-1withRSA"))
	    (t (clc:new-instance (clc:algorithm key))))))
	 
    ;; Transform signature to list form
    (typecase signature
      (ObolVariable
       (setf signature (get-attribute signature "value")))
      (cons
       ;; Assume signature comes from receive.
       (setf signature (first signature))))
    
    ;; Must multiplex on different signature formats
    (setf signature
      (typecase key
	(clc:RSAPublicKey
	 (parse-spki (excl:octets-to-string signature)))
	(t 
	 (mapcar #'(lambda (octvec)
		       (clc:octet-vector-to-integer octvec))
		 (parse-spki (excl:octets-to-string signature))))))
      
    (f1 t "~&eval-verify: signature:~A" signature)
    (f1 t "~&eval-verify: key=~A" key)
    (f1 t "~&eval-verify: data to verify=~A" var)
    (assert (subtypep (type-of var) 'simple-array))
     
    (clc:init-verify verifier key)
    (if (clc:verify verifier signature var)
	t
      (signal 'script-error :script-id (gid) 
		  :msg "SIGNATURE FAILED"))))
  
  
(defun eval-store (exp script-env local-env str)
  "(STORE var path) Write a variable, typically a key to a file."
  (let* ((var (interpreter (second exp) script-env local-env str))
	 (path (third exp))
	 (value (get-attribute var "value")))
    
    (f2 t "store:path=~S" path)
    (assert (equal (type-of var) 'ObolVariable))  
    
    (typecase value
      (clc:SymmetricKey 
       (setf value (clc:key value)))
      (clc:DSAPublicKey
       (setf value (clc:get-encoding value)))
      (clc:DSAPrivateKey
       (setf value (clc:get-encoding value)))
      (clc:RSAPublicKey
       (setf value (clc:get-encoding value)))
      (clc:RSAPrivateKey
       (setf value (clc:get-encoding value)))
      (clc:Diffie-HellmanKey
       (setf value (clc:get-encoding value))))
        
    (with-open-file (filestream path 
		     :direction :output
		     :if-exists :supersede
		     :if-does-not-exist :create)
      (write-sequence value filestream))))
    

(defun eval-load (exp script-env local-env str)
  "(LOAD source attributes) (LOAD) Load a file into an anonymous variable. No transformations, must use believe later on. Default is to load from the application"
  (let* ((source (interpreter (second exp) script-env local-env str))
	 (attributes (third exp))
	 (value))
    (typecase source
      (ObolVariable
       (setf value (find-key source)))       
      (string
       (setf value (get-file-as-byte-array source)))
      (t
      (setf value (receive-msg-from-application (gid)))
	(when value
	  (setf value (car (parse-spki value)))
	  (f1 t "~&load[~A]: Value after parsing=~S" (gid) value))
	
	;; Assume the application never wants to send nil to the script
	(unless value
	  (f3 t "~&load[~A]: No message. Yielding..." (gid))
	  (undo-and-yield)
	  ;; Set a timer, default timeout 40 seconds. Use long timeout (3600)
	  ;; for testing.
	  (set-timer (get-current-script-id)
		     (or (second (assoc 'timeout attributes)) 3600))
	  ;; Must return something in case undo-and-yield returns non-nil. 
	  ;; Otherwise
	  ;; the return value from undo-and-yield is returned. If receive is
	  ;; nested inside a print statement, this value will be printed to
	  ;; the output stream. If we use continuation style for the output
	  ;; stream as well we can avoid this. Return value used by 
	  ;; eval-print.
	  (setf value *yield*))))
    
    value))
    
(defun find-key (address)
  "address is an address string. Simulates database lookup."
  (cond
   ((or (exps-equal-p address "A")	
	(exps-equal-p address "gaupe.cs.uit.no")
	(exps-equal-p address "gaupe.cs.uit.no:9000"))
    (get-file-as-byte-array "prog/Kas.key"))
   ((or (exps-equal-p address "B")
	(exps-equal-p address "nb75.stud.cs.uit.no")
    	(exps-equal-p address "nb75.stud.cs.uit.no:9000"))
    (get-file-as-byte-array "prog/Kbs.key"))
   (t
    (f3 t "~&find-key: Unkown address=~A" address)
    (signal 'script-error :script-id (gid) :msg "FIND KEY: unknown address"))))

(defun get-file-as-byte-array (path)
  (with-open-file (filestream path :direction :input)
    (let ((array (make-array (file-length filestream))))
      (read-sequence array filestream)
      array)))

  
  
  
(defun eval-xor (exp script-env local-env str)
  "(XOR var1 var2) Do non-destructive xor on octet vectors and returns the result."
  (let ((exp1 (interpreter (second exp) script-env local-env str))
	(exp2 (interpreter (third exp) script-env local-env str))
	(val1)
	(val2))
    (if (equal (type-of exp1) 'ObolVariable)
	(setf val1 (get-attribute exp1 "value"))
      (setf val1 exp1))
    (if (equal (type-of exp2) 'ObolVariable)
	(setf val2 (get-attribute exp2 "value"))
      (setf val2 exp2))
    (xor val1 val2)))


(defun eval-nth  (exp script-env local-env str)
  "(NTH n list)"
  (let ((n (second exp))
	(lst (interpreter (third exp) script-env local-env str)))
    ;; Special case if yield.
   (f0 t "~&eval-nth: n=~A lst=~A stringp lst=~A" n lst (stringp lst))
   (if (yield-p lst)
       (progn 
	 (f1 t "~&eval-nth: yielding.")
	 *yield*)
     (nth n lst))))

(defun eval-list (exp script-env local-env str)
  "(LIST exp1 exp2 ...)"
  (f1 t "~&eval-list: exp=~A" exp)
  (mapcar #'(lambda (expr) (interpreter expr script-env local-env str))
	  (rest exp)))

(defun eval-bind (exp script-env local-env str)
  " (BIND var value) Create new binding inside a let scope. NB! var is not evaluated."
  (let ((var (second exp))
	(value (interpreter (third exp) script-env local-env str)))
    (set-var var value script-env local-env)))
  

(defun eval-while (exp script-env local-env str)
  " (WHILE test do-stuff) "
  (let ((res))
    (while (interpreter (second exp) script-env local-env str)
      (setf res (interpreter (third exp) script-env local-env str))
      (if (yield-p res)
	  (return-from eval-while *yield*)
	res))))

(defun eval-print (exp script-env local-env str)
  " (PRINT object) "
  (let ((object (interpreter (second exp) script-env local-env str)))
    ;; hack to avoid printing the result if a nested receive is yielding.
    (if (yield-p object)
	*yield*
      (format str "~&~A" object))))

(defun eval-format (exp script-env local-env str)
  "(FORMAT format-string obj1 obj2 ...) The format string is not evaluated."
  (let* ((format-string (second exp))
	 (objects (cddr exp))
	 (evaluated-objects 
	  (mapcar #'(lambda (obj)
		      (interpreter obj script-env local-env str))
		  objects)))
    ;; Signal a yield and do nothing if one of the nested elements yielded
    (dolist (obj evaluated-objects)
      (when (yield-p obj)
	(return-from eval-format *yield*)))
    
    ;; Use the string value if any of the evaluated objects are strings
    (setf evaluated-objects
      (mapcar 
       #'(lambda (obj)
	   (typecase obj
	     (ObolVariable 
	      (if (string= (get-attribute obj "type") "string")
		  (get-attribute obj "value")
		obj))
	     (t obj)))
       evaluated-objects))
    
    (apply #'format str format-string evaluated-objects)))


(defun eval-send (exp script-env local-env str)
  "(SEND receiver element1 element2 ...)
   (SEND A 3 4 5)"
  (let* ((receiver (interpreter (second exp) script-env local-env str))
	 (msg-elements 
	  (mapcar #'(lambda (element)
		      (interpreter element script-env local-env str))
		  (cddr exp))))
    ;; Assert that we have a non-nil message
    (assert (and msg-elements (car msg-elements)) () 
      "eval-send:No message, msg-elements=~S" msg-elements)
    
    ;; Special case if evaluation of cleartext yielded in load etc: just abort
    (when (yield-p msg-elements)
      (return-from eval-send *yield*))

    
    ;; Extract the address string from any ObolVariables. receiver should
    ;; be a list of strings.
    (unless (listp receiver)
      (setf receiver (list receiver)))
    (setf receiver
      (mapcar 
       #'(lambda (hint)
	   (typecase hint
	     (ObolVariable (get-attribute hint "value"))
	     (t hint)))
       receiver))
    
    (f1 t "~&eval-send: rec=~A msg-elements=~&~A" receiver msg-elements)
    ;; This returns the message.
    (send-msg receiver (format-msg-for-send msg-elements))))
       

;; Rule: every stage does as much as possible with a message with repsect to
;; formatting or deformatting.

(defun eval-receive (exp script-env local-env str)
  "(RECEIVE pattern1 pattern2 ... &optional attributes) 
   (RECEIVE) no from hints or pattern, accept any message from anyone. eval-receive returns the message elements as a list or as a signle element if there is only one. 
"
  (let* ((attributes (get-attributes-from-expression exp))
	 (msg) 
	 (channel
	  (mapcar #'(lambda (element)
		      (interpreter element script-env local-env str))
		  (cdr (assoc 'channel attributes))))
	 (pattern 
	  (mapcan #'(lambda (element)
		      ;; Ignore the attributes
		      (unless (assoc-p element)
			(list 
			 ;; Assume special evaluation rule for new anon 
			 ;; variables.
			 (interpreter element script-env local-env str))))
		  (cdr exp))))
        
    ;; Extract the address string from any ObolVariables. channel should
    ;; be a list of strings. 
    (setf channel
      (mapcar 
       #'(lambda (hint)
	   (typecase hint
	     (ObolVariable (extract-host (get-attribute hint "value")))
	     (t hint)))
       channel))
		  
    
    (delete-msg-from-cache)
    (while (setf msg (receive-msg channel))
      (f1 t "~&eval-receive: Found possible message: ~& msg=~A" msg)
      ;; Test if message fits pattern
      (f1 t "~&eval-receive: Testing patterns: ~& pattern=~A" pattern)
      ;; Return msg if it matches pattern. Pattern can be nil.
      (if (match-and-assign msg pattern script-env local-env)
	  (progn
	    ;; Deletes msg from queue
	    (commit-receive-msg)
	    ;; Ignore timer in case we blocked earlier
	    (delete-timer (get-current-script-id))
	    ;; Return a single element, not a list when there is only one
	    ;; element in the message.
	    (if (= 1 (length msg))
		(return-from eval-receive (car msg))
	      (return-from eval-receive msg)))
	(progn
	  ;; Must signal the message queue that the current message didn't
	  ;; match. The message queue must advance it's message pointer, 
	  ;; otherwise it will return the same message again and this 
	  ;; will result in an infinite loop.
	  (delete-msg-from-cache))))
    
    
    (f3 t "~&receive[~A]: No (matching) message. Yielding..." (gid))
    (undo-and-yield)
    ;; Set a timer, default timeout 40 seconds. Use long timeout (3600)
    ;; for testing
    (set-timer (get-current-script-id)
	       (or (second (assoc 'timeout attributes)) 3600))
    ;; Must return something in case undo-and-yield returns non-nil. 
    ;; Otherwise
    ;; the return value from undo-and-yield is returned. If receive is
    ;; nested inside a print statement, this value will be printed to
    ;; the output stream. If we use continuation style for the output
    ;; stream as well we can avoid this. Return value used by eval-print.
    *yield*))


(defun match-and-assign (msg pattern script-env local-env)
  "msg = (list element1 element 2 ...) 
pattern = (list Na Nb ...)
Check that all non-anonymous variables are there in the specified order and check that the message has enough elements to fit the anonymous variables. This may leave some message elements unaccounted for and that's allowed. The order of the message elements and the pattern elements do matter. Assign value to anon variables.
"
  (f1 t "~&match-and-assign: ~& msg=~&~A ~& pattern=~&~A" msg pattern)  
  (assert (and (listp msg) (listp pattern)))
  (assert (and msg (car msg)) () "match-and-assign: Empty message, msg=~A" msg)
  
  (when (< (length msg) (length pattern))
    (f1 t "~&WARNING: Pattern has more elements than message: " '+ 
	     "~&   msg=~A ~&   pattern=~A~&No match." msg pattern)
    (return-from match-and-assign nil))
  
  (let ((offset -1)
	(msg-element))
    (dolist (pat pattern)
      (incf offset)
      (setf msg-element (nth offset msg))
      (if (anonymous-variable-p pat)
	  (progn
	    (assert (existp pat script-env local-env))
	    (set-var pat msg-element script-env local-env))  
	(unless (exps-equal-p pat msg-element)
	  (f2 t "~&match-and-assign: Pattern element doesn't match " '+
		   "message element: ~&   msg-element=~&~A ~&   pattern=~&~A" 
		   msg-element pat)  
	  (return-from match-and-assign nil)))))

  (f1 t "~&match-and-assign: OK! msg=~&~A~&matches pattern=~&~A" 
	   msg pattern)
  t)


(defun containp (pat msg)
  (dolist (msg-element msg)
    (when (exps-equal-p msg-element pat)
      (return-from containp t)))
  (f1 t "~&containp:message='~A' did not contain pat=~A" msg pat)
  nil)
      
(defun yield-p (result)
  "Returns true if result represents the yield value"
  (typecase result
    (string
     (string= result *yield*))
    (list
     (dolist (res result nil)
       (when (and (stringp res) (string= res *yield*))
	 (return-from yield-p t))))))

(defun eval-decrypt (exp script-env local-env str)
  " (DECRYPT key ciphertext pattern &optional attributes)
    (DECRYPT key ciphertext)
  The key determines the algorithm. ciphertext is a usb8 BLOB. Returns a list of message elements, not ObolVariables. If there is only one return value, return it alone and not as a list. May create new anonymous variables as a side effect."
  (f1 t "~&eval-decrypt: exp=~A" exp)
  (let* ((key-var (interpreter (second exp) script-env local-env str))
	 (key (get-attribute key-var "value"))
	 (attributes (get-attributes-from-expression exp))
	 (ciphertext (interpreter (third exp) script-env local-env str)))
    
    ;; Special case if evaluation of ciphertext yielded in receive: just abort
    (when (yield-p ciphertext)
      (return-from eval-decrypt *yield*))
    
    (let* ((pattern-elements (cdddr exp))
	   (pattern 
	    (mapcan #'(lambda (element)
			;; Ignore the attributes
			(unless (assoc-p element)
			  (list 
			   (interpreter element script-env local-env str))))
		    pattern-elements))
	   (algorithm (clc:algorithm key))
	   (cipher (clc:new-instance algorithm))
	   (msg-elements))
      
      (f1 t "~&eval-decrypt: pattern=~A" pattern) 
    
      (typecase ciphertext
	;; Get value of ciphertext if ObolVariable
	(ObolVariable 
	 (setf ciphertext (get-attribute ciphertext "value")))
	;; Normally input to decrypt comes from receive, which returns a list 
	;; of message elements. With no further information, just extract 
	;; the first element.
	(list
	 (setf ciphertext (car ciphertext))))

      (assert (subtypep (type-of ciphertext) 'simple-array))
      (f1 t "~&eval-decrypt: ciphertext=~&~A" ciphertext)
      
      (typecase key
	(clc:RSAPublicKey
	 (clc:init-decrypt cipher key))
	(clc:RSAPrivateKey
	 (clc:init-decrypt cipher key))
	(clc:SymmetricKey
	 ;; Use the default iv if no iv given.
	 (if (second (assoc 'iv attributes))
	     (clc:init-decrypt cipher key :iv (second (assoc 'iv attributes)))
	   (clc:init-decrypt cipher key)))
	(t (error "Unknown key")))
      
      (setf msg-elements (parse-spki
			  (octets-to-string 
			   (clc:decrypt cipher ciphertext))))

      ;; Part of old 2-phase commit protocol with receive. This code is
      ;; almost dead: Consider removing.
      (delete-msg-from-cache)	    
      
      (if (match-and-assign msg-elements pattern script-env local-env)
	  (if (= 1 (length msg-elements))
	      (car msg-elements)
	    msg-elements)
	(progn 
	  (f3 t "~&eval-decrypt[~A]: No match after decrypt. " '+ 
	      "ABORTING SCRIPT..." (gid))  
	  (signal 'script-error :script-id (gid) 
		  :msg "DECRYPT NO-MATCH"))))))

(defun eval-encrypt (exp script-env local-env str)
  " (encrypt key var1 var2 ...&optional attributes)
 The key determines the algorithm. Returns a BLOB"
  (f1 t "~&eval-encrypt: exp=~A" exp)
  (let* ((key-var (interpreter (second exp) script-env local-env str))
	 (key (get-attribute key-var "value"))
	 (msg-elements 
	  (mapcan #'(lambda (element)
		      ;; Avoid the attributes
		      (unless (assoc-p element)
			(list (interpreter element script-env local-env str))))
		  (cddr exp)))	 
	 (attributes (get-attributes-from-expression exp))
	 (var (format-msg msg-elements))
	 (algorithm (clc:algorithm key))
	 (cipher (clc:new-instance algorithm)))
    
    ;; Special case if evaluation of cleartext yielded in load etc: just abort
    (when (yield-p msg-elements)
      (return-from eval-encrypt *yield*))

    
    (assert (subtypep (type-of var) 'simple-array))
    (typecase key
      (clc:RSAPublicKey
       (clc:init-encrypt cipher key))
      (clc:RSAPrivateKey
       (clc:init-encrypt cipher key))
      (clc:SymmetricKey
       ;; Use the default iv if no iv given.
       (if (second (assoc 'iv attributes))
	   (clc:init-encrypt cipher key :iv (second (assoc 'iv attributes)))
	 (clc:init-encrypt cipher key)))
      (t (error "Unknown key")))
    (clc:encrypt cipher var)))

(defun get-attributes-from-expression (exp)
  (dolist (e exp)
    (when (assoc-p e)
      (return e))))

(defun assoc-p (alist)
  "Return true if alist is an assoc list with a least one element. "
  (when (listp alist)
    (consp (car alist))))

(defun format-msg (msg-elements)
  "Get byte encoding for each msg element, transform to base64 spki and return msg as a big usb8 vector."
  (string-to-octets
   (to-spki (mapcar #'get-msg-element-encoding msg-elements))))

(defun format-msg-for-send (msg-elements)
  "Get byte encoding for each msg element, transform to base64 spki and return the string"
  (unless (listp msg-elements)
    (setf msg-elements (list msg-elements)))
  (to-spki (mapcar #'get-msg-element-encoding msg-elements)))

(defun get-msg-element-encoding (element)
  ;; Get value if ObolVariable.
  (typecase element
    (ObolVariable
     (setf element (get-attribute element "value"))
     (typecase element
       ;; String must come before simple-array because a string is a simple
       ;; array.
       (string 
	(f1 t "~&get-msg-el-enc:ObolVariable with string.")
	(setf element (string-to-octets element)))
       (simple-array 
	(f1 t "~&get-msg-el-enc:ObolVariable with simple array."))
       (clc:SymmetricKey 
	(f1 t "~&Got SymmetricKey")
	(setf element (clc:key element)))
       (clc:AsymmetricKey 
	(f1 t "~&Got AsymmetricKey")
	(setf element (clc:get-encoding element)))
       (t (error "~&get-msg-element-encoding:Unknown msg type=~A." 
		 (type-of element))))
     element)
    (integer 
     (clc:integer-to-octet-vector element))
    (string 
     (string-to-octets element))
    (simple-array 
     element)
    (t (error "~&get-msg-element-encoding:Unknown msg type=~A." 
	      (type-of element)))))

     
(defun eval-load-script (exp script-env local-env str)
  "(LOAD-SCRIPT 'test.obol')"
  (let ((path (second exp)))
    (mapc #'(lambda (expr) (interpreter expr script-env local-env str))
	  (parse-obol path))))

(defun eval-equal (exp script-env local-env str)
  " (EQUAL exp1 exp2) "
  (f0 t "eval-equal: ~A" exp)
  (let ((exp1 (interpreter (second exp) script-env local-env str))
	(exp2 (interpreter (third exp) script-env local-env str)))
    (f0 t "~&exp1:~A"(subtypep (type-of exp1) 'array))
    (f0 t "~&exp2:~A"(subtypep (type-of exp2) 'array))
    (f0 t "~&exp1 type=~A" (type-of exp1))
    (f0 t "~&exp2 type=~A" (type-of exp2))
    (exps-equal-p exp1 exp2)))

(defun exps-equal-p (exp1 exp2)
  "Used by eval-equal and eval-receive and eval-decrypt. NB! if the first exp is an ObolVariable and the last is not, we try to convert the last to an ObolVariable of the same type as the first, and vice versa. This is useful when exp2 is a byte encoding of an ObolVariable sent over a socket for instance. The last probably only works on nonces. Conversion also useful when one expression is a boolean like t."
  ;; Much nesting because we have to convert the value of one expression
  ;; depending on the value of the other.
  (cond
   ;; ObolVariable + ObolVariable.
   ((and (equal (type-of exp1) 'ObolVariable) 
	 (equal (type-of exp2) 'ObolVariable))
    (f1 t "~&exps-equal-p: Two ObolVariables.")
    (var-equal exp1 exp2))
   
   ;; ObolVariable + exp2 or exp1 + ObolVariable.
   ;; We already know that both can't be ObolVariables.
   ((or (equal (type-of exp1) 'ObolVariable)
	(equal (type-of exp2) 'ObolVariable))
    (let ((obolvar (if (equal (type-of exp1) 'ObolVariable) 
		       exp1 
		     exp2))
	  (exp (if (not (equal (type-of exp1) 'ObolVariable))
		   exp1
		 exp2)))
      (f1 t "~&exps-equal-p: ObolVariable + exp")
      (cond
       ;; array, including string
       ((subtypep (type-of exp) 'array)
	(cond
	 ((string= (get-attribute obolvar "type") "string")
	  (f1 t "~&exps-equal-p: ObolVar of type string.")
	  ;; Try to convert the other exp to a string. exp can be a byte array
	  ;; from receive or a litteral string.
	  (unless (stringp exp)
	    (setf exp (octets-to-string exp)))
	  (var-equal obolvar 
		     (make-ObolVariable :type "string"
					:value exp)))
	  
	 ((string= (get-attribute obolvar "type") "address")
	  (f1 t "~&exps-equal-p: ObolVar of type address.")
	  ;; Try to convert the other exp to a string. exp can be a byte array
	  ;; from receive or a litteral string.
	  (unless (stringp exp)
	    (setf exp (octets-to-string exp)))
	  (var-equal obolvar 
		     (make-ObolVariable :type "address"
					:value exp)))
	 (t 
	  (f1 t "~&exps-equal-p[1170]: ObolVar type='~A'" 
	      (get-attribute obolvar "type"))  
	  (var-equal 
	   obolvar 
	   (make-ObolVariable :type (get-attribute obolvar "type") 
			      :value exp)))))
       ;; boolean
       ((or (eq exp t) (eq exp nil))
	(f1 t "~&exps-equal-p: Got boolean.")
	(equal (get-attribute obolvar "value") exp))
       (t
	(error "exp-equal-p: Unhandled case for exp=~S of type ~A" 
	       exp (type-of exp))))))
   
   ;; exp1 + exp2
   ;; Has to use array and not simple array because the literal #( 3 3)
   ;; has type (array (unsigned-byte 8) (2)). Will not handle a literal string
   ;; and a string as byte array from receive. The same goes for a literal
   ;; integer and an integer encoded as a byte array from receive.
   (t
    (f1 t "~&exps-equal-p: exp + exp.")
    (cond 
     ((and (stringp exp1) (stringp exp2))
      (f1 t "~&exps-equal-p: string + string.")
      (string= exp1 exp2))
     ((and (subtypep (type-of exp1) 'array)
	   (subtypep (type-of exp2) 'array))
      (equalp exp1 exp2))
     (t (equal exp1 exp2))))))
	 

(defun eval-let (exp script-env local-env str)
  (let ((bindings (second exp))
	(body (cddr exp)))
    (interpreter
     `((lambda ,(mapcar #'first bindings) ,@body)
       ,@(mapcar #'second bindings)) script-env local-env str)))
    	   

(defun maybe-add (op exps)
  "Adds op to the beginning of exps if len exps > 1."
  (cond 
   ((null exps) nil)
   ((= (length exps) 1) (first exps))
   (t (cons op exps))))
	
   
(defun eval-lambda (exp script-env local-env str)
  "(LAMBDA (x y) (+ x y))"
  (let ((parms (second exp))
	(code (maybe-add 'progn (cddr exp)))) ;progn necess? yes, for defun.
    #'(lambda (&rest args)
	(interpreter 
	 code script-env (extend-local-env parms args local-env) str))))

(defun eval-progn (exp script-env local-env str)
  "Special case to handle yield"
  (last1 (mapcar #'(lambda (y) 
		     (let ((res))
		       (setf res (interpreter y script-env local-env str))
		       (when (yield-p res)
			 (return-from eval-progn *yield*))
		       res))
		 (rest exp))))

(defun eval-if (exp script-env local-env str)
  (if (interpreter (second exp) script-env local-env str)
      (interpreter (third exp) script-env local-env str)
    (interpreter (fourth exp) script-env local-env str)))



(defun eval-function (exp script-env local-env str)
  "Function evaluation: Give access to selected Common Lisp functions."
  (f0 t "~&eval-function: '~A'" exp)
  (let ((fun (interpreter (first exp) script-env local-env str)))
    (when (equal (type-of fun) 'ObolVariable)
      (setq fun (get-attribute fun "value")))
    (apply fun 
	   (mapcar #'(lambda (arg) (interpreter arg script-env local-env str))
		   (rest exp)))))
	

(defun eval-believe (exp script-env local-env str) 
  "(BELIEVE var value type attributes) (BELIEVE K value shared-key ((alg AES) (size 128))) Name can also be a list of names.
BELIEVE has dynamic scope. BELIEVE checks the validity of timestamps."
  (let ((name (second exp))
	(obol-value (interpreter (third exp) script-env local-env str))
	(type (fourth exp))
	(attributes (fifth exp))
	(value))

    (f1 t "~&eval-believe:type=~A(~A)." type (type-of type))
    ;; Extract value if value is an ObolVariable, typically an
    ;; anonymous variable. But not if there is no attributes and type,
    ;; because we then assume the value comes from a nested generate.
    (typecase obol-value
      (ObolVariable
       (if (and (not type) (not attributes))
	   (setf value obol-value
		 type 'ObolVariable)	     
	 (setf value (get-attribute obol-value "value"))))
      (list
       (if (and (not type) (not attributes))
	   ;; Assumes the list contains ObolVariables
	   (setf value obol-value
		 type 'ObolVariable)	     
	 (setf value obol-value)))       
      (t (setf value obol-value)))    

    ;; Special case if evaluation of value yielded in receive: just abort
    (when (yield-p value)
      (return-from eval-believe *yield*))    
    
    ;; Convert value depending on type. Uses equal, not
    ;; stringp because type is a symbol. Must also use a symbol for the 
    ;; cond-key for the same reason. 
    (case type
      (ObolVariable
       ;; We come from generate. Special case because the values are
       ;; already ObolVariables.
       (return-from eval-believe
	 (if (consp name)
	     ;; Return the value of the last variable created.
	     (car (last (mapcar #'(lambda (name-x value-x)
				    (add-value name-x value-x 
					       script-env local-env))
				name value)))
	   (add-value name value script-env local-env))))
      
      (shared-key
       (let ((algorithm (string (second (assoc 'alg attributes)))))
	 (assert (stringp algorithm) () 
	   "Need a valid shared-key alg, alg=~A of type=~A" 
	   algorithm (type-of algorithm))
	 (f1 t "~&eval-believe: Converting value=~&~A ~&of type=~A " '+
		  "to ~A shared-key." value (type-of value) algorithm)
	 (typecase value
	   ;; Comes from generate. Do nothing, value OK
	   (clc:SymmetricKey
	    (f1 t 
		     "~&eval-believe: value is already of type SymmetricKey"))
	   (simple-array 
	    (unless (check-size-p value attributes)
	      ;; signal an error to the application and abort
	      (signal 'script-error :script-id (gid) :msg "INVALID SIZE"))
	      
	    ;; Check the size of the value
	    (setf value (clc:make-SymmetricKey value algorithm)))
	   (t (error "cannot convert value of type=~A" (type-of value))))))

      (timestamp
       ;; If we got here from generate, do nothing.
       (unless (second (assoc 'from-generate attributes))
	 (unless (verify-timestamp value attributes)
	   (signal 'script-error :script-id (gid) :msg "INVALID TIMESTAMP"))))
     
      ((signature)
       (error "eval-believe: type signature is deprecated. Use public-key or private-key instead."))
      
      ((public-key private-key)
       (let* ((alg (second (assoc 'alg attributes)))
	      ;; Determine keytype.
	      (keytype	 
	       (case type
		 ((public-key)
		  (case alg
		    ((RSA) 'clc:RSAPublicKey)
		    ((DSA) 'clc:DSAPublicKey)))
		 ((private-key)
		  (case alg
		    ((RSA) 'clc:RSAPrivateKey)
		    ((DSA) 'clc:DSAPrivateKey))))))

	 ;; Hack because excl:octets-to-string used in 
	 ;; clc:construct-from-encoding in CLC_common.lisp is very 
	 ;; particular with the array type.
	 (setf value (coerce value '(simple-array (unsigned-byte 8) (*))))
	 (when value
	   (setf value (clc:key-from-encoding keytype value)))))	       
	      
      ((diffie-hellman)
       ;; Hack because excl:octets-to-string used in 
       ;; clc:construct-from-encoding in CLC_common.lisp is very 
       ;; particular with the array type.
       (f1 t "~&eval-believe:value=~A(~A)." value (type-of value))
       (setf value 
	 (coerce value '(simple-array (unsigned-byte 8) (*))))
       (setf value (clc:key-from-encoding 'clc:Diffie-HellmanKey value)))	       
	   
      ((address string)
       (f1 t "~&eval-believe: Converting value=~&~A" '+
		"~&of type=~A to address." value (type-of value))
       ;; Only need to convert if the value is not a string.
       (typecase value
	 (string 
	  (f1 t "~&eval-believe: address: value is already a string."))
	 (array
	  (f1 t "~&eval-believe: address value is a array.")
	  ;; NB! (believe A address #(65 0)) won't work at the prompt if we 
	  ;; don't coerce it because the reader doesn't return the correct type
	  (setf value (octets-to-string 
		       (coerce value '(simple-array (unsigned-byte 8) (*))))))
	 (t
	  (f2 t "~&eval-believe: unknown type value=~A (~A)" value (type-of value))
	  (error "eval-believe: unknown value"))))
     
      (nonce
       (if (nonce-ok-p value attributes)
	   ;; Store nonce
	   (script-add-nonce (gid) value)
	 (signal 'script-error :script-id (gid) :msg "INVALID NONCE")))
       
      (t 
       (f1 t "~&eval-believe: Unknown type. Converting value=~&~A" '+
		"~&of type=~A to new type=~A." value (type-of value) type)))

    (add-value name
	       (make-ObolVariable :type type 
				  :value value 
				  :attributes attributes)
	       script-env
	       local-env)))


(defun add-value (name value script-env local-env)
  (if (existp name script-env local-env)
      ;; Change value of existing variable
      (set-var name value script-env local-env)
    ;; Create new variable in the environment.
    (extend-script-env (list name) (list value) script-env))
  value)
  
  
(defun nonce-ok-p (value attributes)
  "Check that the nonce is at least as long as some specified minimum. Also check that this script hasn't seen the nonce before."
  (let ((nonce-ok t)
	(size (second (assoc 'size attributes))))
    (when size
      (when (> size (* 8 (length value)))
	(setf nonce-ok nil)))
    (when (script-old-nonce-p (gid) value)
      (setf nonce-ok nil))
    nonce-ok))
        
(defun check-size-p (value attributes)
  "Assert that the value is an array with enough bits"
  (let ((size (second (assoc 'size attributes))))
    (if size
	(= size (* 8 (length value)))
      t)))      

(defun eval-generate (exp script-env local-env str)
  "(GENERATE type attributes) (GENERATE shared-key ((alg AES) (size 128)))"
  (let* ((type (second exp))
	 (attributes (third exp))
	 (value 
	  (case type
	    (shared-key (generate-key attributes))
	    (nonce (generate-nonce attributes))
	    (public-key (generate-key attributes))
	    (timestamp (get-timestamp attributes))
	    (signature (generate-signature attributes))
	    (diffie-hellman (generate-diffie-hellman attributes))
	    (otherwise (error "Unkown type in generate:~A" type)))))

    (case type
      ((public-key signature)
       (interpreter
	;; Must call list to return a list.
	(list 'list 
	      (make-ObolVariable :type type 
				 :value (clc:public value)
				 :attributes attributes)
	      (make-ObolVariable :type type 
				 :value (clc:private value)
				 :attributes attributes))
	script-env local-env str))
      (t
       (make-ObolVariable :type type 
			  :value value 
			  :attributes attributes)))))

(defun generate-diffie-hellman (attributes)
  (let ((size (second (assoc 'size attributes))))
    ;; Must use string because generate-key lies in a different package
    (clc:generate-key "Diffie-Hellman" size)))


(defun generate-signature (attributes)
  "((alg DSA))"
  (let ((type (second (assoc 'alg attributes))))
    ;; Must use string because new-instance lies in a different package
    (clc:generate-key (symbol-name type))))

(defun get-timestamp (attributes)
  "Returns a universal timestamp."
  (let ((lifetime (or (second (assoc 'lifetime attributes)) 3600)))
    (assert (integerp lifetime))
    (f2 t "~&get-timestamp: lifetime=~A" lifetime)
    (clc:integer-to-octet-vector (+ (get-universal-time) lifetime))))

(defun verify-timestamp (timestamp attributes)
  "Verify if the timestamp as a array is still valid. lifetime in seconds."
  (let ((current (get-universal-time))
	(lifetime (or (second (assoc 'lifetime attributes)) 0))
	(stamp (clc:octet-vector-to-integer timestamp)))
    (< current (+ stamp lifetime))))

(defun generate-nonce (attributes)
  (f1 t "~&generate-nonce: attributes=~A" attributes)
  (let* ((bitsize (second (assoc 'size attributes)))
	 (num-octets (floor bitsize 8)))
    (f1 t "~&generate-nonce: size=~A" bitsize)
    (assert (= (mod bitsize 8) 0))
    (clc:random-secure-octets num-octets)))
	

(defun generate-key (attributes)
  "Assume type size, i.e. '((alg AES) (size 128)) or '((alg RSA) (size 512))"
  (let ((type (second (assoc 'alg attributes)))
	(size (second (assoc 'size attributes))))
    (unless size
      (setf size 128))
    (f1 t "~&generate-key: type=~A size=~A" type size)
    ;; Must use string because generate-key lies in a different package
    (clc:generate-key (symbol-name type) size)))


(defun eval-runtime-cmd (keyword script-env local-env)
  ":in and :sv only works with single obol REPL, not the event loop."
  (case keyword
    (:reinit (init))
    (:exit :exit)
    (:help (obol-help))
    (:in (call-inspect script-env))
    (:pe (print-env script-env local-env))
    (:sv (set-verbosity))))


(defun obol-help ()
  "Obol repl help menu
:exit - exit the current obol REPL (the script continues to exist in the 
runtime).
:help - this help menu.
:reinit - re-initialize the lobo runtime where this script is running.
:in - inspect an object.")


(defun call-inspect (script-env)
  "Calls inspect on current environment or another object"
  (format t "~&Call inspect on object (() to inspect environment): ")
  (let ((in (read)))
    (if in
	(inspect in)
      (inspect script-env))))


;;;;;;;
;;; Representing Obol variables.

;; Must use hash table with test= #'equal to function with strings
(defclass ObolVariable ()
  ((ht :accessor ht
       :initform (make-hash-table :test #'equal))))

(defun make-ObolVariable (&key type value attributes )
  "attributes is list of attribute value pairs, i.e. ((alg AES) (s 2)). The attributes must be of type string. "
  (f0 t "~&make-ObolVariable: attributes=~A" attributes)
  (let ((obj (make-instance 'ObolVariable)))
    (set-value obj "type" type)

    ;; Special case if value already is an obol variable. Then we extract the
    ;; value and transform it according to type.
    (typecase value
      (ObolVariable (set-value obj "value" (get-attribute value "value")))
      (t (set-value obj "value" value)))
    
    ;; Set the other attributes. Must use string because the attribute name can
    ;; be a symbol.
    (mapc #'(lambda (pair) (set-value obj (string (first pair)) (second pair)))
	  attributes)
    
    obj))

(defmethod set-value ((obj ObolVariable) attribute value)
  (f0 t "~&set-value: attribute=~A value=~A" attribute value)
  (assert (stringp attribute))
  (setf (gethash attribute (ht obj)) value))


(defmethod get-attribute ((obj ObolVariable) attribute)
  "Usage: (get-attribute var 'value')"
  (f1 t "~&get-attribute: '~A' with value='~A'" 
	   attribute (gethash attribute (ht obj)))
  (gethash attribute (ht obj)))

(defmethod print-object ((obj ObolVariable) stream)
  (format stream "~&<ObolVariable:")
  (maphash #'(lambda (key value) (format stream "~&    ~A ~A" key value))
	   (ht obj))
  (format stream ">"))

(defmethod var-equal ((obj1 ObolVariable) (obj2 ObolVariable))
  "Returns true if two objects are equal for some definition of equal"
  (let ((type1 (get-attribute obj1 "type"))
	(type2 (get-attribute obj2 "type"))
	(value1 (get-attribute obj1 "value"))
	(value2 (get-attribute obj2 "value")))
    ;; Never equal if different type
    (unless (string= type1 type2)
      (return-from var-equal nil))
    
    (case type1
      (shared-key (equalp (clc:key value1) (clc:key value2)))
      (nonce (equalp value1 value2))
      (timestamp (equalp value1 value2))
      (address 
       (string= value1 value2))
      (string
       (string= value1 value2))
      (otherwise (equal value1 value2)))))
 
;;;;;;;;
;;; The interpreter environment.

;; The environment abstraction conceptualy has three interface methods:
;; 1 - A constructor for the empty environment which we call implicitly by 
;; using nil.
;; 2 - extend-env which extends an existing enviroment with new 
;; (variable,value) pairs.
;; 3 - apply-env which uses the environment, either by getting the value of an 
;; existing variable or setting a new value for an existing variable. 
;; apply-env is splitt into the functions get-var, get-global-var,
;; set-global-var and set-var.

(defun empty-env ()
  ())

(defun empty-script-env () 
  ;; hack to avoid problem with call-by-value when we call extend-env
  ;; on an empty environment.
  (extend-script-env '(nil) '(nil) (empty-env)))

(defun get-var (var script-env local-env str)
  "Get value of variable from given or global environment. If we get an uninitialized anonymous variable like *1, create it in the global scope with value null."
  (declare (ignore str));Remove str??
  (f1 t "~&get-var: looking for var='~A'" var)
  (cond
   ((assoc var local-env)
    (progn 
      (f1 t "~&Found var in local env: ~A value=~A" 
	       var (second (assoc var local-env)))
      (second (assoc var local-env))))
   ((assoc var script-env)
    (progn 
      (f1 t "~&Found var in script env: ~A value=~A" 
	       var (second (assoc var script-env)))
      (second (assoc var script-env))))
   ;; Test if we have an anonymous variable and create it. This is required by
   ;; RECEIVE. Return the name, not the value: this is used by
   ;; match-and-assign. Unfortunately
   ;; this breaks the usual sematics of get-var.
   ((anonymous-variable-p var)
    (f1 t "~&Got a new anonymous variable. Create it. Name=~A" var)
    ;; Note that we use the name of the variable as a value. This is used in
    ;; eval-receive.
    (extend-script-env (list var) (list var) script-env)
    var)
   (t (get-global-var var))))

(defun anonymous-variable-p (var)
  ;; var is a symbol?
  (unless (symbolp var)
    (f1 t "~&anonnymous-variable-p: not a symbol var=~A (type=~A)" 
       var (type-of var)))
  (if (symbolp var)
      (string= "*" (symbol-name var) :end2 1)
    nil))
    
  
(defun get-global-var (var)
  (let* ((default "unbound")
	 (val (get var 'global-val default)))
    (if (eq val default)
	(error "Unbound Obol variable: '~A' in global environment" var)
      val)))

(defun set-global-var (var val)
  (setf (get var 'global-val) val))

(defun set-script-global-var (var val script-env)
  "For defun"
  (when (assoc var script-env)
    (f2 t "~&The script-global variable ~A already exist"))
  (setf (second (assoc var script-env)) val))

(defun set-var (var val script-env local-env)
  "Set a variable to a value in the given or global environment"
  (cond
   ((assoc var local-env)
    (setf (second (assoc var local-env)) val))
   ((assoc var script-env)
    (setf (second (assoc var script-env)) val))
   (t (set-global-var var val)))
  val)

(defun existp (var script-env local-env)
  (or (assoc var script-env) (assoc var local-env)))

(defun extend-script-env (vars vals env)
  "Destructivly adds variables and values to environment"
  (nconc env (mapcar #'list vars vals)))

(defun extend-local-env (vars vals env)
  "Creates a new environment as an extension of the input"
  (f0 t "~&extend-local-env: vars='~A' vals='~A'" vars vals) 
  (nconc (mapcar #'list vars vals) env))

(defun print-local-env (local-env)
  (format t "~&Local environment:")
  (dolist (var local-env)
    (format t "~& ~A ~A" (first var) (second var))))
     
(defun print-env (script-env local-env)
  (format t "~&Script-global environment:")
  (dolist (var script-env)
    (format t "~&~A = ~A" (first var) (second var)))
  (format t "~&Local environment:")
  (dolist (var local-env)
    (format t "~&~A = ~A" (first var) (second var))))


;;;;;;;;;;;;;;;;
;; Obol message queue used by SEND and RECEIVE.
;; 
;; Use a commit/abort protocol when doing receive and nested, i.e.
;; (decrypt (receive ..)).
;; When recieve is successful, place a copy of the message in a tmp var. 
;; After decrypt, call delete-msg-from-cache.
;; Assumes that recieve will never be nested inside another receive. 
;; The next receive calls delete-msg-from-cache to remove any message 
;; cached in a successful receive not nested inside a decrypt.

(defclass Message ()
  ((addr 
    :accessor addr 
    :initarg :addr)			;list with send or receive hints.
   (created 
    :accessor created 
    :initform (get-universal-time))
   (elements
    :accessor elements
    :initarg :elements)))

(defclass MessageQueue ()
  ((lst :accessor lst 
	:initform nil)
   (current-msg :accessor current-msg 
		:initform -1)
   (cache :accessor cache 
	  :initform nil)))

(defun make-MessageQueue ()
  (make-instance 'MessageQueue))

(defun delete-msg-from-cache ()
  "Called after decrypt and at the beginning of receive. Must be reentrant because eval-receive will call it on an empty cache if the previous call to eval-receive failed to find a message."
  (setf (cache *in-msg-queue*) nil))

(defun restore-msg-from-cache ()
  "Reinsert a message in the message cache into the in-msg-queue. Must be reentrant."
  (when (cache *in-msg-queue*)
    (add-msg *in-msg-queue* (cache *in-msg-queue*))
    (setf (cache *in-msg-queue*) nil)))

(defun deliver-messages ()
  "Sender side delivery of messages."
  (dolist (msg (lst *out-msg-queue*))
    (route-msg msg))
  (setf (lst *out-msg-queue*) nil)
  (receive-messages))

(defun route-msg (msg)
  "Send a message to destination, either locally or on a remote host. Assume local msg if no port."
  (let ((host (get-host msg))
	(port (get-port msg)))
    (f1 t "~&route-msg: address= ~A:~A" host port)
    (if port
	(progn
	  (f1 t "~&route-msg:msg for external delivery=~S:~S"
		   host port)
	  (setf host (socket:lookup-hostname host))
	  (send-packet :packet (elements msg)
		       :host host 
		       :port port))      
      (progn
	(f1 t "~&route-msg:msg for local delivery.")
	(add-msg *network-msg-queue* msg)))))

(defun valid-ip-addr-p (host-ip)
  (socket:dotted-to-ipaddr host-ip :errorp nil))

(defun receive-messages ()
  (dolist (msg (lst *network-msg-queue*))
    ;; Decode message
    (f1 t "~&receive-msgs: msg before SPKI decode=~&~A" 
	     (elements msg))
    (setf (elements msg) (parse-spki (elements msg)))
    (f1 t "~&receive-msgs: msg after SPKI decode=~&~A" (elements msg))
    (add-msg *in-msg-queue* msg))  
  (setf (lst *network-msg-queue*) nil)
  ;; Signal the event queue that new messages has arrived by
  ;; unblocking scripts blocking on receive.
  (unblock-scripts))

(defun deliver-incoming-packet (receiver msg)
  "Called by the GOT-PACKET event handler. Do the same as send-msg for local packets. msg: an encoded message as one big string"
  (f1 t "~&deliver-incoming-packet: receiver= ~S msg=~&~S" receiver msg)
  (add-msg *network-msg-queue* (make-Message receiver msg))
  (receive-messages))
  

(defmethod add-msg ((queue MessageQueue) (msg Message))
  (setf (lst queue) (insert-list 0 (lst queue) msg)))

(defmethod find-msg ((queue MessageQueue) channel)
  "Removes msg when found. channel = (list hint1 hint2 ...) or a single hint not in a list. If receiver is nil, return first available message."
  (f1 t "~&find-msg: channel=~&~A" channel)
  
  ;; Part of 2-phase commit protocol within DECRYPT:
  (restore-msg-from-cache)
  ;; Note that use of -1 matches commit protocol
  (do* ((count (1+ (current-msg queue))  (1+ count))
	(size (length (lst queue)))
	(msg (nth count (lst queue)) (nth count (lst queue))))
      ;; If no matching message found, reset state and return nil.
      ((>= count size) 
       (setf (current-msg queue) -1)
       (f1 t "~&find-msg:Searched all messages, but no match." )
       nil)
    
    (f1 t "~&find-msg:Testing msg[count=~A]=~&~A. ~&Size queue=~A" 
	     count msg size)

    ;; Return message if match, otherwise loop and try next message.
    (when (source-match-p msg channel)
      (f1 t "~&find-msg: found msg=~&~A~&for hints=~&~A"
	       msg channel)
      ;; Part of 2-phase commit protocol within RECEIVE:
      (setf (current-msg queue) count)
      ;; Part of 2-phase commit protocol within DECRYPT:
      (setf (cache queue) msg)
      (f1 t "~&find-msg: current-msg=~A" (current-msg queue))
      (return-from find-msg (elements msg)))))
	
     
(defmethod print-object ((obj MessageQueue) stream)
  (format stream "~&<MessageQueue:")
  (format stream "~& size=~A" (length (lst obj)))
  (format stream "~& current-msg=~A" (current-msg obj))
  (format stream "~& cached msg: ~A"
	  (when (cache obj)
	    (format nil "timestamp=~A" (created (cache obj)))))
  (format stream "~& messages=~&")
  (mapcar #'(lambda (msg)
	      (format stream "~&~A" msg))
	  (lst obj))
  (format stream ">"))


(defun receive-msg (channel)
  "Return first message that matches the receive hints or nil if no message found. Must take care not to enter infinite loop when all messages have been tried."
  (f1 t "~&receive-msg: channel=~A" channel)
  (find-msg *in-msg-queue* channel))

(defun commit-receive-msg ()
  (setf (lst *in-msg-queue*)
    (delete-list (current-msg *in-msg-queue*) (lst *in-msg-queue*)))
  (setf (current-msg *in-msg-queue*) -1))
	       
	       
(defun send-msg (receiver msg)
  "Add a new message. msg: an encoded message as one big string."
  (f1 t "~&send-msg: receiver= ~S msg=~S" receiver msg)
  (add-msg *out-msg-queue* (make-Message receiver msg))

  ;; Simulate message routing and possibly network trafic by tranferring from
  ;; out-queue to in-queue.
  (deliver-messages)
  msg)

	 
(defun make-Message (addr msg-elements)
  (unless (listp addr)
    (setf addr (list addr)))
  (make-instance 'Message :addr addr :elements msg-elements))

(defmethod source-match-p ((obj Message) channel)
  ;; When channel=nil it matches any source.
  (unless channel
    (return-from source-match-p t))
  (unless (listp channel)
    (setf channel (list channel)))
  (f1 t "~&source-match-p: addr=~A channel=~A" (addr obj) channel)
  ;; NB! nil is a subset of any list.
  (intersection channel (addr obj) :test #'equal))

(defmethod print-object ((obj Message) stream)
  ;; Note ~& before msg to avoid getting arrays printed with a single element
  ;; per line.
  (format stream "<Message:~& created=~A ~& addr=~A  ~& msg=~A>" 
	  (created obj) (addr obj) (elements obj)))

(defmethod get-host ((obj Message))
  "Extract host from msg address. Either nil, (host port) or (host:port)"
  (let ((addr (addr obj)))
    (cond
     ((>= (length addr) 2)
      (first addr))
     ((stringp (first addr))
      (extract-host (first addr)))
     (t addr))))

(defmethod get-port ((obj Message))
  "Extract host from msg address. nil, (host port) or (host:port)"
  (let ((addr (addr obj)))
    (cond
     ((>= (length addr) 2)
      (second addr))
     ((stringp (first addr))
      (let ((port (extract-port (first addr))))
	(if port
	    (parse-integer port)
	  nil)))
     (t
      (f3 t "~&get-port: WARNING No port in msg=~&~A" obj)
      addr))))

(defun extract-host (host-port)
  (first (split-regexp ":" host-port)))

(defun extract-port (host-port)
  (second (split-regexp ":" host-port)))


;;;;;;;;;;;;
;;; Lobo packet server

(defvar *packet-server-port* 9000)

(defun start-packet-server (&optional port)
  (if port
      (setf *packet-server-port* port)
    (setf port (incf *packet-server-port*)))
  (f2 t "~&Open port ~A for packet server." port)
  (let ((passive (socket:make-socket :connect :passive
				     :local-port port
				     :reuse-address t)))
    (mp:process-run-function
	`(:name "packet-server" :initial-bindings ((*package* . ',*package*)))
      #'(lambda (pass)
	  (let ((count 0))		;use script id
	    (unwind-protect
		(loop
		  (f2 t "~&Ready to accept packet connection on port ~A" port)
		  (let ((con (socket:accept-connection pass)))
		    (mp:process-run-function	
			`(:name ,(format nil "packet~d" (incf count))
				:initial-bindings ((*package* . ',*package*)))
		      #'handle-packet-connection con)))
	      (ignore-errors (close pass :abort t)))))
      passive)))
    
(defun handle-packet-connection (con)
  (unwind-protect
      (handle-packet (read-line con) 
		     (socket:ipaddr-to-hostname (socket:remote-host con)))   
    (ignore-errors (close con :abort t))))

(defun handle-packet (packet remote-host)
  "Add the packet to the event queue."
  (f2 t "~&handle-packet:Got packet:'~A' of type=~A from ~A" 
      packet (type-of packet) remote-host)
  (add-event "GOT-PACKET" packet remote-host))

(defun send-packet (&key (packet (format-msg-for-send (list #(1 2 3))))
			 (host "localhost")
			 (port *packet-server-port*))
  (let ((s (socket:make-socket 
	    :remote-host host :remote-port port)))
    (format s "~A~%" packet) 
    (close s))) 


(defun xor (v1 v2)
  "Do a non-destructive bytewise logxor on the elements of v1 with the elements of v2 and return the new vector. If v1 is shorter that v2 or longer ignore the differencce."
  (let ((res (make-array (length v1) :element-type '(unsigned-byte 8)
			 :initial-contents v1)))
    (nxor res v2)))

(defun nxor (v1 v2)
  "Do a destructive bytewise logxor on the elements of v1 with the elements of v2 and return the new v1. If v1 is shorter that v2 or longer ignore the differencce."
  (map-into v1 #'(lambda (b1 b2) (logxor b1 b2)) v1 v2))


;;;;;;;;;;;
;;; Print the runtime state of Lobo

(defun print-lobo-state ()
  (format t "~&Event queue:")
  (print *event-queue*)
  (format t "~&Scripts:")
  (print *scripts*)
  (f0 t "~&Messages in out-queue:")
  (f0 t "~A" *out-msg-queue*)
  (format t "~&Messages in in-queue:")
  (print *in-msg-queue*)
  (format t "~&Timeouts:")
  (maphash #'(lambda (id timeout)
	       (format t "~& ~A ~A" id timeout))
	   *active-timers*)
  nil)

(defun pls ()
  (print-lobo-state))


;;;;;;;;;
;;; SPKI encoding

(defun to-spki(msg-elements)
  "(format-as-spki '(#(2 3) #(4 3))) => ((AgM=)(BAM=))"
  (mapc #'(lambda (msg-elm)
	    (assert (subtypep (type-of msg-elm) 'simple-array)))
	msg-elements)
  
  ;; Adds a : because we use the lisp reader when parsing in parse-spki
  ;; and then for instance a string with all numericals like 01010101...
  ;; would be seen as a bignum
  (let ((res
	 (apply #'concatenate 
		(append (list 'string "(")
			(mapcar #'(lambda (msg-elm)
				    ;;(format nil "(~A)" (usb8-.. msg-elm nil))
				    (format nil "(:~A)" (clc:hex msg-elm)))
				msg-elements)
			(list ")")))))
    
    (f1 t "~&to-spki:res=~&~S" res)
    res))

(defun parse-spki (spki-string &key typedp)
  "Return a list of octet vectors if untyped spki. typed uses strings like ((type |bootstrap|) (msg |hei der|)) The second part minus the bars || is easily available through symbol-name."
  (f1 t "~&parse-spki:string=~S of type=~A"
	   spki-string (type-of spki-string))
  
  (if typedp
      (mapcar #'parse-typed-element (read-from-string spki-string))    
    (mapcar #'parse-element (read-from-string spki-string))))

(defun parse-typed-element (spki-element)
  "..(string |bootstrap|)...(string |Hello World!|)...(hex |ff3355aa|)"
  (f0 t "~&parse-typed-element: ~S" spki-element)
  (let ((type (first spki-element))
	(value (second spki-element)))
    (f0 t "~&parse-typed-element: type=~A of type ~A." 
	     type (type-of type))
    (case type
      (string (symbol-name value))
      (hex (clc:hexo (symbol-name value)))
      (hexstring (octets-to-string (clc:hexo (symbol-name value))))
      (t (error "~&parse-typed-element: unknown type ~S" type))))) 
  
  
(defun parse-element (spki-element)
  "(...base64...)"
  (f1 t "~&parse-element: type of spki-element=~&~A" 
	   (type-of spki-element))
  (let ((str (car spki-element))) 
    (typecase str
      (symbol
       (f1 t "~&parse-element: got symbol,name=~A" 
		(symbol-name (car spki-element)))
       (clc:hexo (symbol-name (car spki-element))))
       ;;(base64-string-to-usb8-array (symbol-name (car spki-element))))
      (t 
       (error "~&parse-element: got ~A" (type-of str)))))) 


;;;;;;;;
;;; Return values to the application

(defun return-result (script-id result &key (type "OK"))
  (f1 t "~&return-result:id=~A result=~A" script-id result)
  (script-add-result script-id (list type result)))
    
(defun return-error (script-id error-msg)
  (f1 t "~&return-error:id=~A result=~A" script-id error-msg)
  (return-result script-id error-msg :type "ERROR"))


;;;;;;;;
;;; The Lobo API 
;;;
;;; Used by an application when Lobo is used as a linked library.

(defun get-result (script-id)
  "Can be called by an application using the handle returned from load-script. Returns nil if no result is yet available, the string ERROR if error and otherwise the result object, a list. Asynchron. The caller must check if there was an error."
  (if (script-no-result-p script-id)
      nil
    (script-get-result script-id)))
  
(defun send-msg-to-script (script-id msg)
  "An application can use this to send a message to a script."
  (f1 t "~&send-msg-to-script[~A]: msg=~A (after formating: ~S)" 
      script-id 
      msg 
      (format-msg-for-send msg))
  (add-event "MSG-FROM-APP" script-id (format-msg-for-send msg)))

(defun load-script (script &key rpc-handle from-application)
  "(load-script '((print 55) (print 66)) or (load-script 'test.obol'). Return the script-id."
  (let ((obol-script 
	 (typecase script
	   (string 
	    (make-ObolScript (parse-obol script) 
			     :rpc-handle rpc-handle
			     :from-application from-application))
	   (t (make-ObolScript script 
			       :rpc-handle rpc-handle
			       :from-application from-application)))))
    
    (add-script *scripts* obol-script)
    ;; Use prog1 in case add-event modifies the script in some way.
    (prog1
	(id obol-script)
      (add-event "SCRIPT"))))

(defun continue-script (script-id)
  "Continues a script that has previously been stopped (aborted or blocked). Will retry the last top-level expression."
  (unmark-script-aborted script-id)
  (unblock-script script-id)
  (add-event "SCRIPT"))

(defun kill-script (script-id)
  (mark-script-aborted script-id))