;;;;-*-lisp-*-
;;; The Secure Channel key exchange protocol
;;; (based on  Needham-Schroeder protocol with shared keys).
;;;
;;; Server
;;;
;; m1 A->S: A, B, Na
;; m2 S->A: {Na, B, Kab, {Kab, A}Kbs }Kas
;; m3 A->B: {Kab, A}Kbs
;; m4 B->A: {Nb}Kab
;; m5 A->B: {Nb-1}Kab

;; Initialize script with the secret keys shared with the A and B.
;; Note how we need a priori knowledge about the algorithm because S and B
;; has to use the same type of key.
(believe Kas (load "prog/Kas.key") shared-key ((alg AES)))
(believe Kbs (load "prog/Kbs.key") shared-key ((alg AES)))

(while t
  (progn	
    ;; m1 A->S: A, B, Na
    (receive *1 *2 *3 ((channel "s")))
    (believe A *1 address)
    (believe B *2 address)
    (believe Na *3 nonce)

    (believe Kab (generate shared-key ((alg AES) (size 128))))
    
    ;; m2 S->A: {Na, B, Kab, {Kab, A}Kbs }Kas
    (send "a" (encrypt Kas Na B Kab (encrypt Kbs Kab A)))   
    
    ;; Reset anonymous
    (bind *1 '*1)
    (bind *2 '*2)
    (bind *3 '*3)))