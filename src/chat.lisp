;;;;-*-lisp-*-
;;;;
;;;; Description: Secure chat application using proxy and RPC to Lobo.
;;;; Author: T�le Skogan
;;;; Copyright (C) 2003-2004,
;;;; Distribution:  See the accompanying file LICENSE.
;;;;
;;;; Testing:
;;;; -start server with (lobo '("prog/secure-channel-key-excahnge-s.obol"))
;;;; -start lobo on two other machines
;;;; -connect to chat telnet server: choose b first, then a on the other machine
;;only works for local channels

(in-package chat)

(defvar *chat-port* 3050)

(defun start-chat-telnet-server (&optional port)
  (if port
      (setf *chat-port* port)
    (setf port (incf *chat-port*)))
  (f2 t "~&Open port ~A for chat telnet server." port)
  (let ((passive (socket:make-socket :connect :passive
				     :local-host "127.1"
				     :local-port port
				     :reuse-address t)))
    (mp:process-run-function 
	`(:name "chat-telnet-server" 
		:initial-bindings ((*package* . ',*package*)))
      #'(lambda (pass)
	  (let ((count 0))		;use script id
	    (unwind-protect
		(loop
		  (f2 t "~&Ready to accept connection on port ~A" port)
		  (let ((con (socket:accept-connection pass)))
		    ;; Must bind *package* to current package, otherwise
		    ;; the reader will intern symbols in the default package
		    ;; (cl-user) and not in the same package as the 
		    ;; interpreter.
		    (mp:process-run-function
			`(:name ,(format nil "chat~d" (incf count))
				:initial-bindings
				((*standard-output* . ',*standard-output*)
				 (*package* . ',*package*)
				 (*standard-input* . ',*standard-input*)
				 ,@excl:*cl-default-special-bindings*))
		      #'handle-connection con)))
	      (ignore-errors (close pass :abort t)))))
      passive)))


(defun handle-connection (con)
  (unwind-protect
      (progn 
	(format con 
		"~&Choose (1 chat-a, 2 chat-b):")
	(finish-output con)
	(case (read (make-string-input-stream (read-line con)))
	  (1 (safe-chat con
			"a" 
			"prog/chat-auth-a.obol"
			"localhost"
			rpc:*rpc-server-port*))
	  (2 (safe-chat con 
			"b"
			"prog/chat-auth-b.obol"
			"localhost"
			rpc:*rpc-server-port*))))
    (ignore-errors (close con :abort t))))


;;;;;;;;;;
;;; Client side Lobo proxy

(defclass ClientProxy ()
  ((soc :accessor soc)))			;socket to Lobo
   
(defun make-ClientProxy ()
  (make-instance 'ClientProxy))

(defmethod connect-to-lobo ((obj ClientProxy) host port)
  (setf (soc obj) (socket:make-socket 
		   :remote-host host 
		   :remote-port port)))

(defmethod close-proxy ((obj ClientProxy))
  (f0 t "~&close-proxy: closing socket.")
  (close (soc obj) :abort t))

(defmethod bootstrap ((obj ClientProxy) id)
  "Run Lobo's bootstrap protocol. Exchange credentials with the server. This is a normal authentication protocol and we could use SSL, Kerberos or whatever the client is able to support. For now we run a dummy protocol. The credential ia secrect shared with Lobo, here represented by a string."
  (format (soc obj) "~A~%" id)
  (finish-output (soc obj))
    
  (if (bootstrap-ok-p (read-line (soc obj)))
      (f0 t "~&bootstrap: Bootstrap OK")
    (progn
      (close (soc obj) :abort t)
      (error "bootstrap: Invalid credential."))))

(defun bootstrap-ok-p (msg)
  (match-regexp "OK" msg))

(defmethod send-script ((obj ClientProxy) script)
  "script is an Obol script string."
  ;; Must hex encode the script in case it contains some strange characters
  (f0 t "~&send-script:sending key-exchange-script=~&~A" script)
  (let ((msg (format nil "((hexstring |~A|))" 
		     (clc:hex (string-to-octets script)))))
    (format (soc obj) "~A~%" msg)
    (force-output (soc obj))))

(defmethod wait-for-result ((obj ClientProxy))
  "Await result of script. Return message."
  (parse-spki (read-line (soc obj)) :typedp t))
  

(defmethod do-lobo ((obj ClientProxy) function &rest args)
  "(do send msg). Function is a symbol."
  (let ((msg (first args)))
    (case function
      (send 
       (setf msg (format nil "((string send)(string |~A|))" msg )) 
       (format (soc obj) "~A~%" msg)
       (force-output (soc obj)))
      (t (error "Unknown function ~A of type ~A" 
		function (type-of function))))))


(defmethod init-proxy ((obj ClientProxy) host port auth-script id)
    (f1 t "~&init-proxy:Starting...")
    (connect-to-lobo obj host port)
    (bootstrap obj id)
    (send-script obj auth-script)
    
    (if (string= (read-line (soc obj)) "OK")
	(f1 t "~&init-proxy:Secure channel ready!")
      (error "Key exchange error.")))
          
;;;;;;;;
;;; A secure chat application

(defun file-as-string (path)
  "Return a text file as a string"
  (let ((value ""))
    (with-open-file (stream path :direction :input)
      (do ((line (read-line stream nil)
		 (read-line stream nil)))
	  ((null line) value)
	;; Adds newline to make debugging easier.
	(setf value (concatenate 'string value line (format nil "~%")))))))


(defun safe-chat (con id auth-script-path host port)
  "An secure IRC application using Lobo."
  (let ((p (make-ClientProxy))
	(auth-script (file-as-string auth-script-path)))
    
    (init-proxy p host port auth-script id)      
    (loop
      (format con "~&secure-chat[~A]>" id)
      (force-output con)
      (when (string= (chat-multiplex con p) "exit")
	(close-proxy p)
	(return)))))


(defun chat-multiplex (display-con proxy)
  "Multiplex user input from display and messages from remote Lobo." 
  (let ((msg)
	(remote-con (soc proxy)))
    (dolist (c (mp:wait-for-input-available (list remote-con display-con)))
      (cond
       ;; Data from remote user to local display.
       ((equal c remote-con)
	(setf msg (parse-spki (read-line remote-con) :typedp t))
	(format display-con "~%~A" (car msg))
	(force-output display-con))
       ;; Data from local display to remote user
       ((equal c display-con)
	(setf msg (read-line display-con))   
	;; remove last char (probably \n from telnet's \r\n line termination.
	(setf msg (subseq msg 0 (- (length msg) 1)))
	(if (match-regexp "exit" msg)
	    (progn
	      (do-lobo proxy 'send ":exit")
	      (return-from chat-multiplex "exit"))
	  (do-lobo proxy 'send msg)))
       (t (f3 t "ERROR: Unknown connection ~A" c))))))
