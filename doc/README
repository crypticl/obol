INTRODUCTION
Obol is a special purpose programming language for implementing security 
protocols of the kind used in SSL, SSH and Kerberos. This module implements 
Obol. The implementation consists of a combined runtime and interpreter, 
hereafter called Lobo. It is distributed under a MIT-style license (see the 
LICENSE file).

The implementation depends on the Crypticl library, a cryptographic library 
written in Common Lisp.


WEBSITE
http://common-lisp.net/project/crypticl/


INSTALL
The library can be loaded by loading the file "load.lisp". This file will load
 the library and run unit tests. It requires the crypticl library to be 
available. Change the path in "load.lisp" to point to the location of the 
crypto library.

Lobo has been tested on Allegro 6.2 running on Windows XP. The code requires 
Allegro and will not run on other Common Lisp distributions due to the use of 
Allegro specific sockets and synchronization mechanisms (locks). To load and 
test the system:

cl-user(1): (load "load.lisp")
cl-user(2): (in-package lobo)
lobo(3): (lobo)

This will start the Obol runtime. Start an Obol prompt by:
lobo(4): (obol)

You can load and run a whole Obol program by using load-script:
obol[1]>(load-script "prog/test.obol")

To test a more realistic example, try the Needham-Schroeder protocol
implementation found in prog/needham-schroeder-*.obol. Start a Lobo runtime
on three different machines, get an obol prompt by executing (obol) and
run the protocol step by step for each protocol actor in the three repls (one
Obol program for each actor A, B and S). You'll have to replace the default
local channel addresses by real network addresses in the Obol programs.
Alternatively you can run the protocol with all three actors in the same
Lobo runtime. This will be easier if you use the telnet server built into 
the runtime. Open three telnet connections (default port 9023) and start a new
Obol script in each. Run the protocl as above.

N.B. If you run on Windows and want to use telnet from within emacs, install
cygwin and use cygwin's telnet.exe since Window's telnet doesn't agree with 
emacs's telnet.el. Modify the telnet command used by emacs as described at 
http://www.khngai.com/emacs/cygwin.php.


DOCUMENTATION
Obol and the ideas behind the language are described in a master thesis in the
file thesis.pdf. FOr examples of Obol code look in the prog directory.


CHANGELOG
See the file ChangeLog for the project change log.


CREDITS
Tage Stabell-Kul� (original design of Obol and more)
Frode V. Fjeld 
Per Harald Myrvang (original design of Obol)  


AUTHOR
T�le Skogan tasko@frisurf.no



